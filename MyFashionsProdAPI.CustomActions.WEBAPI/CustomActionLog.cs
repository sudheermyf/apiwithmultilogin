﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyFashionsProdAPI.CustomActions.WEBAPI
{
    public class CustomActionLog
    {
        FileStream fsErrorLog;
        StreamWriter writer;
        string strError = string.Empty;
        public CustomActionLog()
        {

        }

        public void WriteEntry(string message)
        {
            try
            {
                string path = @"C:\myfashions\Logs\";
                if (path.Trim().Length > 0)
                {
                    string DebugLogName = "MyFasionsProdAPI_Service_InstallerLog_" + DateTime.Now.ToString("MMddyyyy") + ".txt";
                    if (!File.Exists(path + DebugLogName))
                    {
                        fsErrorLog = new FileStream(path + DebugLogName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                    }
                    else
                    {
                        fsErrorLog = new FileStream(path + DebugLogName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                    }

                    DateTime now = DateTime.Now;
                    string uTime = now.ToString("u");
                    uTime = uTime.Substring(0, uTime.Length - 1);
                    string timeStamp = string.Format("{0,25} [Thread {1}]: ", uTime + now.ToString(" (ddd) "), Thread.CurrentThread.GetHashCode());

                    writer = new StreamWriter(fsErrorLog);
                    writer.Flush();
                    writer.WriteLine(timeStamp + " " + message);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("CustomActionLog - ", ex.Message + ex.StackTrace);
            }
        }
    }
}
