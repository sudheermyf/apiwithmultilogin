﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.CartLogic
{
    public class CartOperationsLogic
    {
        public List<Product> GetCartItemsUnderEmail(string emailAddress, out string result)
        {
            result = Messages.Succcess;
            try
            {
                List<CartItems> lstCartItems = new List<CartItems>();
                List<CartItems> lstFetchedCart = Apps.dbContext.CartItems.Where(c => c.EMAIL.Equals(emailAddress)).ToList();
                lstFetchedCart.ForEach(c =>
                {
                    CartItems cartItem = PopulateCartItem(c);
                    if (cartItem != null)
                    {
                        lstCartItems.Add(cartItem);
                    }
                });
                Apps.Logger.Info("Cart Items Count: " + lstCartItems.Count);

                return lstCartItems.Select(c => c.RELATEDPRODUCT).ToList();
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("GetCartItemsUnderEmail: " + ex.Message, ex);
            }
            return null;
        }

        public string AddCart(CartItems cartItem)
        {
            try
            {
                Apps.Logger.Info("Initiating AddCart.");
                string result = ValidateCartItem(cartItem, "A");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnCartItems.AddCart(cartItem);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Add Cart Item: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteCart(long productId, string emailAddress)
        {
            try
            {
                emailAddress = emailAddress.Trim();
                Apps.Logger.Info("Initiating DeleteCart.");
                string result = ValidateDeleteCartItem(productId, emailAddress);
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnCartItems.DeleteCart(productId, emailAddress);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Delete Cart Item: " + ex.Message, ex);
                return ex.Message;
            }
        }

        private string ValidateDeleteCartItem(long productId, string emailAddress)
        {
            if (productId > 0 && !string.IsNullOrEmpty(emailAddress))
            {
                if (
                    Apps.dbContext.CartItems.ToList().Exists(
                        c =>
                            c.RELATEDPRODUCT.ID.Equals(productId) &&
                            c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return Messages.Succcess;
                }
                else
                {
                    return Messages.CartItem + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                return Messages.RequiredFieldsEmpty;
            }
        }

        private string ValidateCartItem(CartItems cartItem, string parameter)
        {
            string result = string.Empty;
            switch (parameter)
            {
                case "A":
                    if ((!string.IsNullOrEmpty(cartItem.NAME) && !string.IsNullOrEmpty(cartItem.EMAIL))
                        &&
                        ((cartItem.RELATEDCOMPANY != null && cartItem.RELATEDCOMPANY.ID > 0) ||
                         (cartItem.RELATEDSTORE != null && cartItem.RELATEDSTORE.ID > 0))
                        && (cartItem.RELATEDPRODUCT != null && cartItem.RELATEDPRODUCT.ID > 0
                            &&
                            Apps.dbContext.Product.ToList()
                                .Exists(
                                    c =>
                                        c.ID.Equals(cartItem.RELATEDPRODUCT.ID) &&
                                        c.RELATEDSTORE.REGISTEREDCOMPANY.ID.Equals(cartItem.RELATEDCOMPANY.ID))))
                    {
                        if (
                            Apps.dbContext.CartItems.ToList()
                                .Exists(
                                    c =>
                                        c.EMAIL.Equals(cartItem.EMAIL,
                                            StringComparison.InvariantCultureIgnoreCase) &&
                                        c.RELATEDPRODUCT.ID.Equals(cartItem.RELATEDPRODUCT.ID)))
                        {
                            result = Messages.AlreadyItemExistsinCart;
                        }
                        else
                        {
                            result = Messages.Succcess;
                        }
                    }
                    else
                    {
                        result = Messages.RequiredFieldsEmpty;
                    }
                    break;
                case "D":
                case "U":
                default:
                    result = Messages.InvalidCRUDOperation;
                    break;
            }
            return result;
        }

        private CartItems PopulateCartItem(CartItems cart)
        {
            CartItems cartItem = new CartItems();
            cartItem = PopulateCartFields.AssignCartValues(cartItem, cart);
            cartItem.SNO = cart.SNO;
            cartItem.ID = cart.ID;
            if (cart.RELATEDPRODUCT != null && cart.RELATEDPRODUCT.ID > 0)
            {
                cartItem.RELATEDPRODUCT = PopulateProductFields.AssignProductValues(cartItem.RELATEDPRODUCT, cart.RELATEDPRODUCT, isFromCreate: true, offersOnly: true);
            }
            return cartItem;
        }

        public string DeleteCartItems(string emailAddress)
        {
            try
            {
                emailAddress = emailAddress.Trim();
                Apps.Logger.Info("Initiating DeleteCart.");
                string result = ValidateDeleteCartItems(emailAddress);
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnCartItems.DeleteCartItems(emailAddress);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Delete Cart Item: " + ex.Message, ex);
                return ex.Message;
            }
        }

        private string ValidateDeleteCartItems(string emailAddress)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {
                if (
                    Apps.dbContext.CartItems.Any(
                        c =>
                            c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return Messages.Succcess;
                }
                else
                {
                    return Messages.CartItem + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                return Messages.RequiredFieldsEmpty;
            }
        }
    }
}
