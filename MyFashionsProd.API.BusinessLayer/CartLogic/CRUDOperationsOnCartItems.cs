﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.CartLogic
{
    public class CRUDOperationsOnCartItems
    {
        internal static string AddCart(CartItems cartItem)
        {
            string result = string.Empty;
            if (cartItem != null)
            {
                try
                {
                    CartItems addToCart = new CartItems();
                    addToCart = PopulateCartFields.AssignCartValues(addToCart, cartItem);
                    addToCart.ID = Apps.dbContext.CartItems.ToList().Count > 0 ? Apps.dbContext.CartItems.Max(c => c.ID) + 1 : 1;
                    addToCart.ISACTIVE = true;
                    addToCart.LASTUPDATEDTIME = DateTime.Now;

                    if (cartItem.RELATEDCOMPANY != null && cartItem.RELATEDCOMPANY.ID > 0)
                    {
                        addToCart.RELATEDCOMPANY = Apps.dbContext.Company.FirstOrDefault(c => c.ID.Equals(cartItem.RELATEDCOMPANY.ID));
                    }

                    if (cartItem.RELATEDPRODUCT != null && cartItem.RELATEDPRODUCT.ID > 0)
                    {
                        addToCart.RELATEDPRODUCT = Apps.dbContext.Product.FirstOrDefault(c => c.ID.Equals(cartItem.RELATEDPRODUCT.ID));
                    }

                    if (cartItem.RELATEDSTORE != null && cartItem.RELATEDSTORE.ID > 0)
                    {
                        addToCart.RELATEDSTORE = Apps.dbContext.Stores.First(c => c.ID.Equals(cartItem.RELATEDSTORE.ID));
                    }

                    Apps.dbContext.CartItems.Add(addToCart);
                    Apps.dbContext.SaveChanges();
                    result = Messages.Succcess;
                }
                catch (Exception ex)
                {
                    Apps.Logger.ErrorException("CRUDOperationsOnCartItems -> AddCart: " + ex.Message, ex);
                    result = ex.Message.ToString();
                }
            }
            else
            {
                result = Messages.ObjectShouldNotBeNull;
            }
            return result;
        }

        internal static string DeleteCart(long productId, string emailAddress)
        {
            string result = string.Empty;

            try
            {
                if (
                    Apps.dbContext.CartItems.ToList().Exists(
                        c =>
                            c.RELATEDPRODUCT.ID.Equals(productId) &&
                            c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase)))
                {
                    CartItems cartItem =
                        Apps.dbContext.CartItems.FirstOrDefault(
                            c =>
                                c.RELATEDPRODUCT.ID.Equals(productId) &&
                                c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase));
                    if (cartItem != null)
                    {
                        Apps.dbContext.CartItems.Remove(cartItem);
                        Apps.dbContext.SaveChanges();
                        result = Messages.Succcess;
                    }
                }
                else
                {
                    result = Messages.CartItem + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("CRUDOperationsOnCartItems -> DeleteCart: " + ex.Message, ex);
                result = ex.Message.ToString();
            }

            return result;
        }

        internal static string DeleteCartItems(string emailAddress)
        {
            string result = string.Empty;
            emailAddress = emailAddress.Trim();
            try
            {
                if (Apps.dbContext.CartItems.Any(
                        c =>
                            c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase)))
                {
                    Apps.dbContext.CartItems.RemoveRange(Apps.dbContext.CartItems.Where(c => c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase)));
                    Apps.dbContext.SaveChanges();
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.CartItem + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("CRUDOperationsOnCartItems -> DeleteCartItems: " + ex.Message, ex);
                result = ex.Message.ToString();
            }

            return result;
        }
    }
}
