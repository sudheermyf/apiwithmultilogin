﻿using MyfashionsDB.DBEnums;
using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Offers
{
    public class MasterOffersOperationsLogic
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public MasterOffersOperationsLogic(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
        #region Get All Offers under Main Enum

        public List<MasterOffers> GetAllOffersUnderCompany(long relatedCompanyID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetAllOffersUnderCompany. Related Company ID: " + relatedCompanyID);
            List<MasterOffers> lstOffers = new List<MasterOffers>();
            errorMessage = string.Empty;
            if (relatedCompanyID > 0)
            {
                if (HeaderInformation.CompanyID.Equals(relatedCompanyID))
                {
                     if (Apps.dbContext.MainEnum.ToList().Exists(c => c.NAME.Equals(TopEnums.Offers.ToString())))
                     {
                         MainEnum masterEnum = Apps.dbContext.MainEnum.Where(c => c.ID.Equals((long)TopEnums.Offers) && c.LSTOFFERS.Any(d => d.REGCOMPANY.ID.Equals(relatedCompanyID))).FirstOrDefault();
                         if (masterEnum != null)
                         {
                             masterEnum.LSTOFFERS.Where(c => c.REGCOMPANY.ID.Equals(relatedCompanyID)).ToList().ForEach(c =>
                                 {
                                     MasterOffers offer = new MasterOffers();
                                     offer = PopulateOfferFields.AssignMasterOfferValues(offer, c);
                                     lstOffers.Add(offer);
                                 });
                         }
                     }
                   
                    else
                    {
                        errorMessage = Messages.UnableToFind + " " + Messages.CateogryMsg + " " + Messages.RunInitiateSeed;
                    }
                }
                else
                {
                    errorMessage = Messages.NotAuthorize;
                }
            }
            else
            {
                errorMessage = Messages.Company + " " + Messages.DoesnotExists;
            }

            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);

            return lstOffers;
        }

        public List<MasterOffers> GetAllOffersUnderStore(long relatedStoreID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetAllOffersUnderStore. Related Store ID: " + relatedStoreID);
            List<MasterOffers> lstOffers = new List<MasterOffers>();
            errorMessage = string.Empty;
            if (relatedStoreID > 0)
            {
                if (HeaderInformation.LstStoreIds.Contains(relatedStoreID))
                {
                    Stores storeInfo = Apps.dbContext.Stores.Where(c => c.ID.Equals(relatedStoreID)).FirstOrDefault();
                    if (storeInfo != null)
                    {
                        storeInfo.LSTOFFERS.ToList().ForEach(c =>
                        {
                            MasterOffers offer = new MasterOffers();
                            offer = PopulateOfferFields.AssignMasterOfferValues(offer, c);
                            lstOffers.Add(offer);
                        });
                    }
                    else
                    {
                        errorMessage = Messages.StoreMsg + " " + Messages.DoesnotExists;
                    }
                }
                else
                {
                    errorMessage = Messages.NotAuthorize;
                }
            }
            else
            {
                errorMessage = Messages.StoreMsg + " " + Messages.DoesnotExists;
            }

            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);

            return lstOffers;
        }

        private bool CheckAcccessToStore(long relatedStoreID)
        {
            Company fetchedCompany = Apps.dbContext.Company.FirstOrDefault(c => c.ID.Equals(HeaderInformation.CompanyID));
            if (fetchedCompany.LSTSTORES != null)
            {
                Stores fetchedStore = fetchedCompany.LSTSTORES.FirstOrDefault(c => c.ID.Equals(relatedStoreID));
                if (fetchedStore != null)
                {
                    return true;
                }
            }
            return false;
        }

        public MasterOffers GetOfferByMasterOfferID(long relatedMasterOfferID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetOfferByMasterOfferID. Related Master Offer ID: " + relatedMasterOfferID);
            MasterOffers masterOffer = new MasterOffers();
            masterOffer.LSTSUBOFFERS = new List<MasterOffers>();

            errorMessage = string.Empty;
            if (relatedMasterOfferID > 0)
            {
                if (CheckAccessToMasterOffer(relatedMasterOfferID))
                {
                    MasterOffers fetchedOffer = Apps.dbContext.MasterOffers.Where(c => c.ID.Equals(relatedMasterOfferID)).FirstOrDefault();

                    if (fetchedOffer != null)
                    {
                        masterOffer = PopulateOfferFields.AssignMasterOfferValues(masterOffer, fetchedOffer);
                    }
                    else
                    {
                        errorMessage = Messages.Offer + " " + Messages.DoesnotExists;
                    }
                }
                else
                {
                    errorMessage = Messages.NotAuthorize;
                }
            }
            else
            {
                errorMessage = Messages.Offer + " " + Messages.DoesnotExists;
            }

            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);

            return masterOffer;
        }

        private bool CheckAccessToMasterOffer(long relatedMasterOfferID)
        {
            MasterOffers offer = Apps.dbContext.MasterOffers.FirstOrDefault(c => c.ID.Equals(relatedMasterOfferID));
            if (offer != null)
            {
                if (HeaderInformation.Login.Equals(LoginType.Store))
                {
                    if (offer.LSTREGSTORE.Exists(c => HeaderInformation.LstStoreIds.Contains(c.ID)))
                    {
                        return true;
                    }
                }
                else
                {
                    if (offer.REGCOMPANY.ID.Equals(HeaderInformation.CompanyID))
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }
            return false;
        }
        
        #endregion
        
        public string AddOffer(MasterOffers masterOffer)
        {
            try
            {
                Apps.Logger.Info("Initiating AddOffer");
                string result = ValidateMasterOffer(masterOffer, "A");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnMasterOffers.AddMasterOffer(masterOffer);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch(Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddOffer: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string UpdateOffer(MasterOffers masterOffer)
        {
            try
            {
                Apps.Logger.Info("Initiatng UpdateOffer");
                string result = ValidateMasterOffer(masterOffer, "U");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnMasterOffers.UpdateMasterOffer(masterOffer);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while update offer: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteOffer(MasterOffers masterOffer)
        {
            try
            {
                Apps.Logger.Info("Initiating Delete Offer");
                string result = ValidateMasterOffer(masterOffer, "D");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnMasterOffers.DeleteMasterOffer(masterOffer);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Delete Offer: " + ex.Message, ex);
                return ex.Message;
            }
        }

        private string ValidateMasterOffer(MasterOffers masterOffer, string code)
        {
            if (masterOffer != null)
            {
                switch (code)
                {
                    case "A":
                    #region Adding Validation
                        if (masterOffer != null && !string.IsNullOrEmpty(masterOffer.DISCOUNTTYPE) && !string.IsNullOrEmpty(masterOffer.IMAGELOCATION)
                            && !string.IsNullOrEmpty(masterOffer.NAME) && (masterOffer.REGCOMPANY != null || (masterOffer.LSTREGSTORE != null && masterOffer.LSTREGSTORE.Count > 0)))
                        {
                            if (Apps.dbContext.MasterOffers.ToList().Exists(c => c.NAME.Equals(masterOffer.NAME)))
                                return Messages.Offer + " " + Messages.AlreadyExists;
                            else
                                return Messages.Succcess;
                        }
                        else
                        {
                            return Messages.RequiredFieldsEmpty;
                        }
                    #endregion
                    case "U":
                        #region Updating Validation
                        if (masterOffer != null && masterOffer.ID > 0 && masterOffer.SNO > 0 && !string.IsNullOrEmpty(masterOffer.DISCOUNTTYPE) && !string.IsNullOrEmpty(masterOffer.IMAGELOCATION)
                            && !string.IsNullOrEmpty(masterOffer.NAME) && (masterOffer.REGCOMPANY != null || (masterOffer.LSTREGSTORE != null && masterOffer.LSTREGSTORE.Count > 0)))
                        {
                             return Messages.Succcess;
                        }
                        else
                        {
                            return Messages.RequiredFieldsEmpty;
                        }
                        #endregion
                    case "D":
                        #region Deleting Validation
                        if (masterOffer != null && masterOffer.ID > 0 && masterOffer.SNO > 0 && !string.IsNullOrEmpty(masterOffer.DISCOUNTTYPE) && !string.IsNullOrEmpty(masterOffer.IMAGELOCATION)
                            && !string.IsNullOrEmpty(masterOffer.NAME) && (masterOffer.REGCOMPANY != null || (masterOffer.LSTREGSTORE != null && masterOffer.LSTREGSTORE.Count > 0)))
                        {
                            if (!Apps.dbContext.MasterOffers.ToList().Exists(c => c.NAME.Equals(masterOffer.NAME)))
                                return Messages.Offer + " " + Messages.DoesnotExists;
                            else
                                return Messages.Succcess;
                        }
                        else
                        {
                            return Messages.RequiredFieldsEmpty;
                        }
                        #endregion
                    default:
                        return Messages.UnKnown;
                }
            }
            else
                return Messages.InvalidObject;
        }
    }
}
