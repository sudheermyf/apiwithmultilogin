﻿using MyfashionsDB.Models;
using MyFashionsProd.API.Models.PriceRange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.PriceRangeSlider
{
    public class PriceRangeSliderOperationsLogic
    {
        public PriceSlider GetPriceUnderStoreId(long storeId, out string errorMessage)
        {
            errorMessage = string.Empty;
            Apps.Logger.Info("Initiating GetPriceUnderStoreId. Store ID: " + storeId);
            PriceSlider priceSlider = new PriceSlider();
            Stores fetchedStore = Apps.dbContext.Stores.FirstOrDefault(c => c.ID.Equals(storeId));
            if (storeId > 0 && fetchedStore != null && fetchedStore.LSTPRODUCTS != null)
            {
                priceSlider.StartPrice = fetchedStore.LSTPRODUCTS.Min(c => c.PRICE);
                priceSlider.EndPrice = fetchedStore.LSTPRODUCTS.Max(c => c.PRICE);
            }
            else
            {
                errorMessage = Messages.CateogryMsg + " " + Messages.DoesnotExists;
            }
            Apps.Logger.Info((!string.IsNullOrEmpty(errorMessage)) ? ("Error Message: " + errorMessage) : ("Result: " + Messages.Succcess));
            return priceSlider;
        }

        public PriceSlider GetPriceUnderCategoryId(long categoryId, out string errorMessage)
        {
            errorMessage = string.Empty;
            Apps.Logger.Info("Initiating GetPriceUnderCategoryId. Category ID: " + categoryId);
            PriceSlider priceSlider = new PriceSlider();
            MasterCategories fetchedCategory = Apps.dbContext.MasterCategories.FirstOrDefault(c => c.ID.Equals(categoryId));
            if (categoryId > 0 && fetchedCategory != null && fetchedCategory.LSTPRODUCTS != null)
            {
                priceSlider.StartPrice = fetchedCategory.LSTPRODUCTS.Where(c=>c.ISACTIVE).Min(c => c.PRICE);
                priceSlider.EndPrice = fetchedCategory.LSTPRODUCTS.Where(c => c.ISACTIVE).Max(c => c.PRICE);
            }
            else
            {
                errorMessage = Messages.ProductMsg + " " + Messages.NotFound;
            }
            Apps.Logger.Info((!string.IsNullOrEmpty(errorMessage)) ? ("Error Message: " + errorMessage) : ("Result: " + Messages.Succcess));
            return priceSlider;
        }
        public PriceSlider GetPriceUnderOffer(long offerId, out string errorMessage)
        {
            errorMessage = string.Empty;
            Apps.Logger.Info("Initiating GetPriceUnderOffer. Offer ID: " + offerId);
            PriceSlider priceSlider = new PriceSlider();
            MasterOffers fetchedOffer = Apps.dbContext.MasterOffers.FirstOrDefault(c => c.ID.Equals(offerId));
            if (offerId > 0 && fetchedOffer != null && fetchedOffer.LSTPRODUCTS != null)
            {
                priceSlider.StartPrice = fetchedOffer.LSTPRODUCTS.Where(c => c.ISACTIVE).Min(c => c.PRICE);
                priceSlider.EndPrice = fetchedOffer.LSTPRODUCTS.Where(c => c.ISACTIVE).Max(c => c.PRICE);
            }
            else
            {
                errorMessage = Messages.ProductMsg + " " + Messages.NotFound;
            }
            Apps.Logger.Info((!string.IsNullOrEmpty(errorMessage)) ? ("Error Message: " + errorMessage) : ("Result: " + Messages.Succcess));
            return priceSlider;
        }

        public PriceSlider GetPriceUnderArrivalDays(long arrivalDays,long categoryId ,long storeId,out string errorMessage)
        {
            errorMessage = string.Empty;
            Apps.Logger.Info("Initiating GetPriceUnderArrivalDays. Arrival Days: " + arrivalDays + "CategoryId" + categoryId + "StoreId"+storeId+"");
            PriceSlider priceSlider = new PriceSlider();
            DateTime checkedDateTime = DateTime.Now.AddDays(-arrivalDays);
            List<Product> lstFetchedProducts = Apps.dbContext.Product.Where(c => c.LASTUPDATEDTIME >= checkedDateTime && c.RELATEDSTORE.ID.Equals(storeId) && c.RELATEDCATEGORIES.Any(d=>d.ID.Equals(categoryId)) ).ToList();
            if (arrivalDays > 0 && lstFetchedProducts != null && lstFetchedProducts.Count > 0)
            {
                priceSlider.StartPrice = lstFetchedProducts.Where(c => c.ISACTIVE).Min(c => c.PRICE);
                priceSlider.EndPrice = lstFetchedProducts.Where(c => c.ISACTIVE).Max(c => c.PRICE);
            }
            else
            {
                errorMessage = Messages.ProductMsg + " " + Messages.NotFound;
            }
            Apps.Logger.Info((!string.IsNullOrEmpty(errorMessage)) ? ("Error Message: " + errorMessage) : ("Result: " + Messages.Succcess));
            return priceSlider;
        }
    }
}
