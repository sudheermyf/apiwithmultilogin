﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer
{
    public class LoginInfoCache
    {
        public LoginInfoCache()
        {
            LstStoreIds = new List<long>();
            Login = LoginType.Store;
        }
        public long CompanyID { get; set; }

        //public static long StoreID { get; set; }
        public List<long> LstStoreIds { get; set; }

        public string CompanyName { get; set; }
        public string StoreName { get; set; }

        public LoginType Login { get; set; }
  
    }
}
