﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateCompanyFields
    {
        public static Company AssignCompanyValues(Company mainCompanyObj, Company source)
        {
            if (mainCompanyObj == null)
                mainCompanyObj = new Company();

            if (source != null)
            {
                mainCompanyObj.ADDRES1 = source.ADDRES1;
                mainCompanyObj.ADDRESS2 = source.ADDRESS2;
                mainCompanyObj.CITY = source.CITY;
                mainCompanyObj.COMPANYID = source.COMPANYID;
                mainCompanyObj.ID = source.ID;
                mainCompanyObj.IMAGELOCATION = source.IMAGELOCATION;
                mainCompanyObj.IMAGENAME = source.IMAGENAME;
                mainCompanyObj.ISACTIVE = source.ISACTIVE;
                mainCompanyObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                mainCompanyObj.NAME = source.NAME;
                mainCompanyObj.NUMEMPLOYEES = source.NUMEMPLOYEES;
                mainCompanyObj.PASSWORD = source.PASSWORD;
                mainCompanyObj.STATE = source.STATE;
                mainCompanyObj.USERNAME = source.USERNAME;
                mainCompanyObj.VERSION = source.VERSION;
                mainCompanyObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
            }
            return mainCompanyObj;
        }
    }
}
