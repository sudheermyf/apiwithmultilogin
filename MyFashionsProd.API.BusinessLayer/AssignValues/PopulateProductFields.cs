﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateProductFields
    {
        public static Product AssignProductValues(Product masterProd, Product source, bool isFromCreate = false, bool offersOnly = false)
        {
            var asyncTask = Task.Run(() =>
                    {
                        if (masterProd == null)
                            masterProd = new Product();
                        if (source != null)
                        {
                            masterProd.ADDITIONALCHARGES = source.ADDITIONALCHARGES;
                            masterProd.AVAILABLEQUANTITY = source.AVAILABLEQUANTITY;
                            masterProd.DESCRIPTION = source.DESCRIPTION;
                            masterProd.ID = source.ID;
                            masterProd.IMAGELOCATION = source.IMAGELOCATION;
                            masterProd.IMAGENAME = source.IMAGENAME;
                            masterProd.ISACTIVE = source.ISACTIVE;
                            masterProd.ITEMCODE = source.ITEMCODE;
                            masterProd.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                            masterProd.NAME = source.NAME;
                            masterProd.PRICE = source.PRICE;
                            masterProd.PRICECALCUSINGFILTERS = source.PRICECALCUSINGFILTERS;
                            masterProd.PRODUCTTYPE = source.PRODUCTTYPE;
                            masterProd.SNO = source.SNO;
                            masterProd.TEMPIMAGELOCATION = source.TEMPIMAGELOCATION;
                            masterProd.VERSION = source.VERSION;
                            masterProd.WASTAGE = source.WASTAGE;
                            masterProd.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
                            masterProd.WEIGHT = source.WEIGHT;

                            if (!isFromCreate)
                            {
                                if (masterProd.RELATEDSTORE == null)
                                {
                                    masterProd.RELATEDSTORE = new Stores();
                                }
                                masterProd.RELATEDSTORE = PopulateStoreFields.AssignStoreValues(masterProd.RELATEDSTORE, source.RELATEDSTORE, false);

                                if (masterProd.RELATEDBRAND == null)
                                {
                                    masterProd.RELATEDBRAND = new Brand();
                                }
                                masterProd.RELATEDBRAND = PopulateBrandFields.AssignBrandValues(masterProd.RELATEDBRAND, source.RELATEDBRAND, fetchProducts: false, fetchCompany: false);
                                #region Populate Fiters
                                if (source.RELATEDFILTERS != null && source.RELATEDFILTERS.Count > 0)
                                {
                                    if (masterProd.RELATEDFILTERS == null)
                                        masterProd.RELATEDFILTERS = new List<MasterFilters>();

                                    source.RELATEDFILTERS.ToList().ForEach(d =>
                                        {
                                            MasterFilters filter = new MasterFilters();
                                            filter = PopulateFiltersFields.AssignFilterValues(filter, d, fetchStoreInfo: false, fetchSubCategs: false, fetchCompany: false);
                                            masterProd.RELATEDFILTERS.Add(filter);
                                        });
                                }
                                #endregion

                                #region Populate Categories
                                if (source.RELATEDCATEGORIES != null && source.RELATEDCATEGORIES.Count > 0)
                                {
                                    if (masterProd.RELATEDCATEGORIES == null)
                                        masterProd.RELATEDCATEGORIES = new List<MasterCategories>();

                                    source.RELATEDCATEGORIES.ToList().ForEach(d =>
                                        {
                                            MasterCategories categ = new MasterCategories();
                                            categ = PopulateCategoryFields.AssignSubCategoryValues(categ, d, fetchFilters: false, fetchProducts: false, fetchCompany: false);
                                            masterProd.RELATEDCATEGORIES.Add(categ);
                                        });
                                }
                                #endregion

                                #region Populate Similar Products
                                if (source.LSTSIMILARPRODUCTS != null && source.LSTSIMILARPRODUCTS.Count > 0)
                                {
                                    if (masterProd.LSTSIMILARPRODUCTS == null)
                                        masterProd.LSTSIMILARPRODUCTS = new List<SimilarProduct>();

                                    source.LSTSIMILARPRODUCTS.ToList().ForEach(d =>
                                        {
                                            SimilarProduct simProd = new SimilarProduct();
                                            simProd = PopulateSimilarProductFields.AssignSimilarProductValues(simProd, d);
                                            masterProd.LSTSIMILARPRODUCTS.Add(simProd);
                                        });
                                }
                                #endregion

                                #region Populate Complete Look
                                if (source.LSTCOMPLETELOOK != null && source.LSTCOMPLETELOOK.Count > 0)
                                {
                                    if (masterProd.LSTCOMPLETELOOK == null)
                                        masterProd.LSTCOMPLETELOOK = new List<CompleteLook>();

                                    source.LSTCOMPLETELOOK.ToList().ForEach(d =>
                                    {
                                        CompleteLook completeLook = new CompleteLook();
                                        completeLook = PopulateCompleteLookFields.AssignCompleteLookValues(completeLook, d);
                                        masterProd.LSTCOMPLETELOOK.Add(completeLook);
                                    });
                                }
                                #endregion

                                #region Populate Product Image Collection
                                if (source.LSTPRODUCTIMAGES != null && source.LSTPRODUCTIMAGES.Count > 0)
                                {
                                    if (masterProd.LSTPRODUCTIMAGES == null)
                                        masterProd.LSTPRODUCTIMAGES = new List<ProductImageCollection>();

                                    source.LSTPRODUCTIMAGES.ToList().ForEach(d =>
                                        {
                                            ProductImageCollection prodImg = new ProductImageCollection();
                                            prodImg = PopulateProductImageCollectionFields.AssignProductImageCollectionValues(prodImg, d);
                                            masterProd.LSTPRODUCTIMAGES.Add(prodImg);
                                        });
                                }
                                #endregion

                                #region Populate Offers
                                if (source.LSTOFFERS != null && source.LSTOFFERS.Count > 0)
                                {
                                    if (masterProd.LSTOFFERS == null)
                                        masterProd.LSTOFFERS = new List<MasterOffers>();

                                    source.LSTOFFERS.ToList().ForEach(d =>
                                        {
                                            MasterOffers offer = new MasterOffers();
                                            offer = PopulateOfferFields.AssignMasterOfferValues(offer, d, fetchStores: false, fetchSubCategs: false, fetchProducts: false, fetchCompany: false);
                                            masterProd.LSTOFFERS.Add(offer);
                                        });
                                }
                                #endregion

                                #region Populate Filter Related Calculation
                                if (source.FILTERSRELATEDTOCALC != null && source.FILTERSRELATEDTOCALC.Count > 0)
                                {
                                    if (masterProd.FILTERSRELATEDTOCALC == null)
                                        masterProd.FILTERSRELATEDTOCALC = new List<FilterCalculation>();

                                    source.FILTERSRELATEDTOCALC.ToList().ForEach(d =>
                                        {
                                            FilterCalculation filterCalc = new FilterCalculation();
                                            filterCalc = PopulateFilterCalculationFields.AssignFilterCalculationFields(filterCalc, d);
                                            masterProd.FILTERSRELATEDTOCALC.Add(filterCalc);
                                        });
                                }
                                #endregion

                            }

                            if (offersOnly)
                            {
                                #region Populate Offers
                                if (source.LSTOFFERS != null && source.LSTOFFERS.Count > 0)
                                {
                                    if (masterProd.LSTOFFERS == null)
                                        masterProd.LSTOFFERS = new List<MasterOffers>();

                                    source.LSTOFFERS.ToList().ForEach(d =>
                                        {
                                            MasterOffers offer = new MasterOffers();
                                            offer = PopulateOfferFields.AssignMasterOfferValues(offer, d, fetchStores: false, fetchSubCategs: false, fetchProducts: false, fetchCompany: false);
                                            masterProd.LSTOFFERS.Add(offer);
                                        });
                                }
                                #endregion
                            }
                        }
                    });
            asyncTask.Wait();
            return masterProd;
        }
    }
}