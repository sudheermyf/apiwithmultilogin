﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateCategoryFields
    {
        public static MasterCategories AssignSubCategoryValues(MasterCategories masterCategObj, MasterCategories source,
            bool fetchFilters = true, bool fetchProducts = true, bool fetchCompany = true)
        {
            if (masterCategObj == null)
                masterCategObj = new MasterCategories();

            if (source != null)
            {
                masterCategObj.ID = source.ID;
                masterCategObj.IMAGELOCATION = source.IMAGELOCATION;
                masterCategObj.IMAGENAME = source.IMAGENAME;
                masterCategObj.ISACTIVE = source.ISACTIVE;
                masterCategObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterCategObj.NAME = source.NAME;
                masterCategObj.SNO = source.SNO;
                masterCategObj.VERSION = source.VERSION;
                masterCategObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
                if (source.SELECTEDENUM != null && source.SELECTEDENUM.ID > 0)
                {
                    masterCategObj.SELECTEDENUM = PopulateEnumFields.AssignIndividualEnumValues(masterCategObj.SELECTEDENUM, source.SELECTEDENUM);
                }
                if (source.SELECTEDCATEG != null && source.SELECTEDCATEG.ID > 0)
                {
                    masterCategObj.SELECTEDCATEG = AssignIndividualSubCategoryValues(masterCategObj.SELECTEDCATEG, source.SELECTEDCATEG, fetchCompany: false);
                }
                if (source.REGCOMPANY != null && source.REGCOMPANY.ID > 0 && fetchCompany)
                {
                    masterCategObj.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterCategObj.REGCOMPANY, source.REGCOMPANY);
                }

                if (source.LSTFILTERS != null && fetchFilters)
                {
                    if (masterCategObj.LSTFILTERS == null)
                    {
                        masterCategObj.LSTFILTERS = new List<MasterFilters>();
                    }
                    source.LSTFILTERS.ToList().ForEach(c =>
                    {
                        MasterFilters masFilter = new MasterFilters();
                        masFilter = AssignValues.PopulateFiltersFields.AssignFilterValues(masFilter, c, true);
                        masterCategObj.LSTFILTERS.Add(masFilter);
                    });
                }
                if (source.LSTPRODUCTS != null && fetchProducts)
                {
                    if (masterCategObj.LSTPRODUCTS == null)
                        masterCategObj.LSTPRODUCTS = new List<Product>();

                    source.LSTPRODUCTS.ToList().ForEach(c =>
                    {
                        Product prod = new Product();
                        prod = AssignValues.PopulateProductFields.AssignProductValues(prod, c, isFromCreate: true);


                        if (c.RELATEDFILTERS.Count > 0)
                        {
                            c.RELATEDFILTERS.ToList().ForEach(d =>
                            {
                                MasterFilters masFilter = new MasterFilters();
                                masFilter = AssignValues.PopulateFiltersFields.AssignFilterValues(masFilter, d, true);
                                if (prod.RELATEDFILTERS == null)
                                    prod.RELATEDFILTERS = new List<MasterFilters>();
                                prod.RELATEDFILTERS.Add(masFilter);
                            });

                        }
                        #region Populate Product Image Collection
                        if (c.LSTPRODUCTIMAGES != null && c.LSTPRODUCTIMAGES.Count > 0)
                        {
                            c.LSTPRODUCTIMAGES.ToList().ForEach(d =>
                            {
                                if (prod.LSTPRODUCTIMAGES == null)
                                    prod.LSTPRODUCTIMAGES = new List<ProductImageCollection>();

                                ProductImageCollection prodImg = new ProductImageCollection();
                                prodImg = PopulateProductImageCollectionFields.AssignProductImageCollectionValues(prodImg, d);
                                prod.LSTPRODUCTIMAGES.Add(prodImg);
                            });
                        }
                        #endregion

                        Stores store = new Stores();
                        store = AssignValues.PopulateStoreFields.AssignStoreValues(store, c.RELATEDSTORE);
                        prod.RELATEDSTORE = store;

                        masterCategObj.LSTPRODUCTS.Add(prod);
                    });
                }
                if (source.LSTSUBCATEGORIES != null)
                {
                    if (masterCategObj.LSTSUBCATEGORIES == null)
                        masterCategObj.LSTSUBCATEGORIES = new List<MasterCategories>();

                    source.LSTSUBCATEGORIES.ToList().ForEach(c =>
                    {
                        MasterCategories mainSubCategory = new MasterCategories();
                        mainSubCategory = AssignSubCategoryValues(mainSubCategory, c, fetchFilters: false, fetchProducts: false);
                        masterCategObj.LSTSUBCATEGORIES.Add(mainSubCategory);
                    });
                }
            }
            return masterCategObj;
        }

        private static MasterCategories AssignIndividualSubCategoryValues(MasterCategories masterCategObj, MasterCategories source, bool fetchCompany = true)
        {
            if (masterCategObj == null)
                masterCategObj = new MasterCategories();

            if (source != null)
            {
                masterCategObj.ID = source.ID;
                masterCategObj.IMAGELOCATION = source.IMAGELOCATION;
                masterCategObj.IMAGENAME = source.IMAGENAME;
                masterCategObj.ISACTIVE = source.ISACTIVE;
                masterCategObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterCategObj.NAME = source.NAME;
                masterCategObj.SNO = source.SNO;
                masterCategObj.VERSION = source.VERSION;
                masterCategObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
                if (fetchCompany)
                {
                    masterCategObj.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterCategObj.REGCOMPANY, source.REGCOMPANY);
                }

            }
            return masterCategObj;
        }

        public static MasterCategories AssignCreateMasterCategoriesValues(MasterCategories masterCategObj, MasterCategories source)
        {
            if (masterCategObj == null)
                masterCategObj = new MasterCategories();

            if (source != null)
            {
                masterCategObj.ID = source.ID;
                masterCategObj.IMAGELOCATION = source.IMAGELOCATION;
                masterCategObj.IMAGENAME = source.IMAGENAME;
                masterCategObj.ISACTIVE = source.ISACTIVE;
                masterCategObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterCategObj.NAME = source.NAME;
                //masterCategObj.SNO = source.SNO;
                masterCategObj.VERSION = source.VERSION;
                masterCategObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
            }
            return masterCategObj;
        }
    }
}