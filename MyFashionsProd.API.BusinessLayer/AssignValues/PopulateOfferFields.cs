﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateOfferFields
    {
        public static MasterOffers AssignMasterOfferValues(MasterOffers masterOfferObj, MasterOffers source,
            bool fetchProducts = true, bool fetchSubCategs = true, bool fetchStores = true, bool fetchCompany = true)
        {
            if (masterOfferObj == null)
                masterOfferObj = new MasterOffers();

            if (source != null)
            {
                masterOfferObj.ID = source.ID;
                masterOfferObj.NAME = source.NAME;
                masterOfferObj.BUYVALUE = source.BUYVALUE;
                masterOfferObj.DESCRIPTION = source.DESCRIPTION;
                masterOfferObj.DISCOUNT = source.DISCOUNT;
                masterOfferObj.DISCOUNTTYPE = source.DISCOUNTTYPE;
                masterOfferObj.GETVALUE = source.GETVALUE;
                masterOfferObj.IMAGELOCATION = source.IMAGELOCATION;
                masterOfferObj.IMAGENAME = source.IMAGENAME;
                masterOfferObj.ISEXPIRED = source.ISEXPIRED;
                masterOfferObj.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                masterOfferObj.SNO = source.SNO;
                masterOfferObj.TEMPIMAGELOCATION = source.TEMPIMAGELOCATION;
                masterOfferObj.VERSION = source.VERSION;
                masterOfferObj.WEBIMAGELOCATION = source.WEBIMAGELOCATION;

                if (fetchCompany)
                {
                    masterOfferObj.REGCOMPANY = PopulateCompanyFields.AssignCompanyValues(masterOfferObj.REGCOMPANY, source.REGCOMPANY);
                }

                if (source.LSTPRODUCTS != null && fetchProducts)
                {
                    if (masterOfferObj.LSTPRODUCTS == null)
                        masterOfferObj.LSTPRODUCTS = new List<Product>();

                    source.LSTPRODUCTS.ToList().ForEach(c =>
                    {
                        Product prod = new Product();
                        prod = AssignValues.PopulateProductFields.AssignProductValues(prod, c, isFromCreate: true);


                        if (c.RELATEDFILTERS.Count > 0)
                        {
                            c.RELATEDFILTERS.ToList().ForEach(d =>
                            {
                                MasterFilters masFilter = new MasterFilters();
                                masFilter = AssignValues.PopulateFiltersFields.AssignFilterValues(masFilter, d, true);
                                if (prod.RELATEDFILTERS == null)
                                    prod.RELATEDFILTERS = new List<MasterFilters>();
                                prod.RELATEDFILTERS.Add(masFilter);
                            });

                        }
                        #region Populate Product Image Collection
                        if (c.LSTPRODUCTIMAGES != null && c.LSTPRODUCTIMAGES.Count > 0)
                        {
                            c.LSTPRODUCTIMAGES.ToList().ForEach(d =>
                            {
                                if (prod.LSTPRODUCTIMAGES == null)
                                    prod.LSTPRODUCTIMAGES = new List<ProductImageCollection>();

                                ProductImageCollection prodImg = new ProductImageCollection();
                                prodImg = PopulateProductImageCollectionFields.AssignProductImageCollectionValues(prodImg, d);
                                prod.LSTPRODUCTIMAGES.Add(prodImg);
                            });
                        }
                        #endregion

                        #region Populate Offers

                        if (c.LSTOFFERS != null && c.LSTOFFERS.Count > 0)
                        {
                            if (prod.LSTOFFERS == null)
                                prod.LSTOFFERS = new List<MasterOffers>();

                            c.LSTOFFERS.ToList().ForEach(d =>
                            {
                                MasterOffers offer = new MasterOffers();
                                offer = PopulateOfferFields.AssignMasterOfferValues(offer, d, fetchStores: false, fetchSubCategs: false, fetchProducts: false, fetchCompany: false);
                                prod.LSTOFFERS.Add(offer);
                            });
                        }
                        #endregion
                        Stores store = new Stores();
                        store = AssignValues.PopulateStoreFields.AssignStoreValues(store, c.RELATEDSTORE);
                        prod.RELATEDSTORE = store;

                        masterOfferObj.LSTPRODUCTS.Add(prod);
                    });
                }
                if (source.SELECTEDOFFER != null)
                {
                    masterOfferObj.SELECTEDOFFER = AssignFieldsForCreateOffer(masterOfferObj.SELECTEDOFFER, source.SELECTEDOFFER);
                }

                if (source.SELECTEDENUM != null)
                {
                    masterOfferObj.SELECTEDENUM = PopulateEnumFields.AssignIndividualEnumValues(masterOfferObj.SELECTEDENUM, source.SELECTEDENUM);
                }

                if (source.LSTREGSTORE != null && fetchStores)
                {
                    List<Stores> lstStores = new List<Stores>();
                    source.LSTREGSTORE.ForEach(d =>
                    {
                        Stores store = new Stores();
                        store = PopulateStoreFields.AssignStoreValues(store, d);
                        lstStores.Add(store);
                    });
                    masterOfferObj.LSTREGSTORE = lstStores;
                }
                if (source.LSTAPPLIEDSUBCATEGORIES != null && fetchSubCategs)
                {
                    if (masterOfferObj.LSTAPPLIEDSUBCATEGORIES == null)
                        masterOfferObj.LSTAPPLIEDSUBCATEGORIES = new List<MasterCategories>();

                    source.LSTAPPLIEDSUBCATEGORIES.ToList().ForEach(c =>
                    {

                        MasterCategories masterCateg = new MasterCategories();
                        masterCateg = PopulateCategoryFields.AssignCreateMasterCategoriesValues(masterCateg, c);
                        masterOfferObj.LSTAPPLIEDSUBCATEGORIES.Add(masterCateg);
                    });

                }
                if (source.LSTSUBOFFERS != null)
                {
                    List<MasterOffers> lstSubOffers = new List<MasterOffers>();
                    source.LSTSUBOFFERS.ToList().ForEach(d =>
                    {
                        MasterOffers masterOffer = new MasterOffers();
                        masterOffer = AssignMasterOfferValues(masterOffer, d, fetchStores: false, fetchSubCategs: false);
                        lstSubOffers.Add(masterOffer);
                    });
                    masterOfferObj.LSTSUBOFFERS = lstSubOffers;
                }
            }
            return masterOfferObj;
        }
        public static MasterOffers AssignFieldsForCreateOffer(MasterOffers mainMasterOffer, MasterOffers source)
        {
            if (mainMasterOffer == null)
                mainMasterOffer = new MasterOffers();

            if (source != null)
            {
                mainMasterOffer.SNO = source.SNO;
                mainMasterOffer.BUYVALUE = source.BUYVALUE;
                mainMasterOffer.DESCRIPTION = source.DESCRIPTION;
                mainMasterOffer.DISCOUNT = source.DISCOUNT;
                mainMasterOffer.DISCOUNTTYPE = source.DISCOUNTTYPE;
                mainMasterOffer.GETVALUE = source.GETVALUE;
                mainMasterOffer.ID = source.ID;
                mainMasterOffer.IMAGELOCATION = source.IMAGELOCATION;
                mainMasterOffer.IMAGENAME = source.IMAGENAME;
                mainMasterOffer.ISEXPIRED = source.ISEXPIRED;
                mainMasterOffer.LASTUPDATEDTIME = DateTime.Now;
                mainMasterOffer.NAME = source.NAME;
                mainMasterOffer.TEMPIMAGELOCATION = source.TEMPIMAGELOCATION;
                mainMasterOffer.VERSION = source.VERSION;
                mainMasterOffer.WEBIMAGELOCATION = source.WEBIMAGELOCATION;

                if (mainMasterOffer.LSTAPPLIEDSUBCATEGORIES == null)
                    mainMasterOffer.LSTAPPLIEDSUBCATEGORIES = new List<MasterCategories>();
                if (mainMasterOffer.LSTPRODUCTS == null)
                    mainMasterOffer.LSTPRODUCTS = new List<Product>();
                if (mainMasterOffer.LSTREGSTORE == null)
                    mainMasterOffer.LSTREGSTORE = new List<Stores>();
                if (mainMasterOffer.LSTSUBOFFERS == null)
                    mainMasterOffer.LSTSUBOFFERS = new List<MasterOffers>();
                if (mainMasterOffer.REGCOMPANY == null)
                    mainMasterOffer.REGCOMPANY = new Company();
            }
            return mainMasterOffer;
        }
    }
}