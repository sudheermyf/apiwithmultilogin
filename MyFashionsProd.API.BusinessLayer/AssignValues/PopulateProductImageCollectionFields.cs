﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateProductImageCollectionFields
    {
        public static ProductImageCollection AssignProductImageCollectionValues(ProductImageCollection prodImgCollect, ProductImageCollection source)
        {
            if (prodImgCollect == null)
                prodImgCollect = new ProductImageCollection();

            if (source != null)
            {
                prodImgCollect.ID = source.ID;
                prodImgCollect.IMAGELOCATION = source.IMAGELOCATION;
                prodImgCollect.IMAGENAME = source.IMAGENAME;
                prodImgCollect.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                prodImgCollect.SNO = source.SNO;
                prodImgCollect.TEMPIMAGELOCATION = source.TEMPIMAGELOCATION;
                prodImgCollect.VERSION = source.VERSION;
                prodImgCollect.WEBIMAGELOCATION = source.WEBIMAGELOCATION;
            }
            return prodImgCollect;
        }
    }
}
