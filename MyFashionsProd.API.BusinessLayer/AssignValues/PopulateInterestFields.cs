﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateInterestFields
    {
        public static ExpressInterest AssignInterestValues(ExpressInterest mainExpressInterest, ExpressInterest source)
        {
            if (mainExpressInterest == null)
            {
                mainExpressInterest = new ExpressInterest();
            }

            if (source != null)
            {
                mainExpressInterest.ADDRESS = source.ADDRESS;
                mainExpressInterest.ALTERNATECONTACT = source.ALTERNATECONTACT;
                mainExpressInterest.CITY = source.CITY;
                mainExpressInterest.CONTACTNUMBER = source.CONTACTNUMBER;
                mainExpressInterest.COUNTRY = source.COUNTRY;
                mainExpressInterest.EMAIL = source.EMAIL;
                mainExpressInterest.GENDER = source.GENDER;
                mainExpressInterest.ISACTIVE = source.ISACTIVE;
                mainExpressInterest.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                mainExpressInterest.NAME = source.NAME;
                mainExpressInterest.NOTES = source.NOTES;
                mainExpressInterest.STATE = source.STATE;
                mainExpressInterest.USERNAME = source.USERNAME;
                mainExpressInterest.VERSION = source.VERSION;
            }
            return mainExpressInterest;
        }
    }
}
