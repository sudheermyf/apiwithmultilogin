﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateCartFields
    {
        public static CartItems AssignCartValues(CartItems mainCartItem, CartItems source)
        {
            if (mainCartItem == null)
            {
                mainCartItem = new CartItems();
            }

            if (source != null)
            {
                mainCartItem.ADDRESS = source.ADDRESS;
                mainCartItem.ALTERNATECONTACT = source.ALTERNATECONTACT;
                mainCartItem.CITY = source.CITY;
                mainCartItem.CONTACTNUMBER = source.CONTACTNUMBER;
                mainCartItem.COUNTRY = source.COUNTRY;
                mainCartItem.EMAIL = source.EMAIL;
                mainCartItem.GENDER = source.GENDER;
                mainCartItem.ISACTIVE = source.ISACTIVE;
                mainCartItem.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                mainCartItem.NAME = source.NAME;
                mainCartItem.NOTES = source.NOTES;
                mainCartItem.STATE = source.STATE;
                mainCartItem.USERNAME = source.USERNAME;
                mainCartItem.VERSION = source.VERSION;
            }
            return mainCartItem;
        }
    }
}
