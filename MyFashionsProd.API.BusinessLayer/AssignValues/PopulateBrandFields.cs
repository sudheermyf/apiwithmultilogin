﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.AssignValues
{
    public class PopulateBrandFields
    {
        public static Brand AssignBrandValues(Brand mainBrand, Brand source, bool fetchProducts = true, bool fetchCompany = true)
        {
            if (mainBrand == null)
                mainBrand = new Brand();

            if (source != null)
            {
                mainBrand.BRANDNAME = source.BRANDNAME;
                mainBrand.DESCRIPTION = source.DESCRIPTION;
                mainBrand.ID = source.ID;
                mainBrand.IMAGELOCATION = source.IMAGELOCATION;
                mainBrand.IMAGENAME = source.IMAGENAME;
                mainBrand.ISACTIVE = source.ISACTIVE;
                mainBrand.LASTUPDATEDTIME = source.LASTUPDATEDTIME;
                mainBrand.SNO = source.SNO;
                mainBrand.VERSION = source.VERSION;
                mainBrand.WEBIMAGELOCATION = source.WEBIMAGELOCATION;

                if (fetchCompany)
                {
                    mainBrand.SELECTEDCOMPANY = PopulateCompanyFields.AssignCompanyValues(mainBrand.SELECTEDCOMPANY, source.SELECTEDCOMPANY);
                }
                if (source.LSTPRODUCTS != null && fetchProducts)
                {
                    if (mainBrand.LSTPRODUCTS == null)
                        mainBrand.LSTPRODUCTS = new List<Product>();

                    source.LSTPRODUCTS.ToList().ForEach(c =>
                    {
                        Product prod = new Product();
                        prod = AssignValues.PopulateProductFields.AssignProductValues(prod, c, isFromCreate:true);


                        if (c.RELATEDFILTERS.Count > 0)
                        {
                            c.RELATEDFILTERS.ToList().ForEach(d =>
                            {
                                MasterFilters masFilter = new MasterFilters();
                                masFilter = AssignValues.PopulateFiltersFields.AssignFilterValues(masFilter, d, true);
                                if (prod.RELATEDFILTERS == null)
                                    prod.RELATEDFILTERS = new List<MasterFilters>();
                                prod.RELATEDFILTERS.Add(masFilter);
                            });

                        }
                        #region Populate Product Image Collection
                        if (c.LSTPRODUCTIMAGES != null && c.LSTPRODUCTIMAGES.Count > 0)
                        {
                            c.LSTPRODUCTIMAGES.ToList().ForEach(d =>
                            {
                                if (prod.LSTPRODUCTIMAGES == null)
                                    prod.LSTPRODUCTIMAGES = new List<ProductImageCollection>();

                                ProductImageCollection prodImg = new ProductImageCollection();
                                prodImg = PopulateProductImageCollectionFields.AssignProductImageCollectionValues(prodImg, d);
                                prod.LSTPRODUCTIMAGES.Add(prodImg);
                            });
                        }
                        #endregion

                        Stores store = new Stores();
                        store = AssignValues.PopulateStoreFields.AssignStoreValues(store, c.RELATEDSTORE);
                        prod.RELATEDSTORE = store;

                        mainBrand.LSTPRODUCTS.Add(prod);
                    });
                }
            }
            return mainBrand;
        }
    }
}