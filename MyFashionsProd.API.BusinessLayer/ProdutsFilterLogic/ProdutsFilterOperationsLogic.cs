﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using MyFashionsProd.API.Models.Enums;
using MyFashionsProd.API.Models.ProductsFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.ProdutsFilterLogic
{
    public class ProdutsFilterOperationsLogic
    {
        public List<Product> GetProductsByFilter(IntegratedFilter integratedFilter, int pageNumber, out string result)
        {
            try
            {
                result = Messages.Succcess;
                List<Product> lstProducts = new List<Product>();
                IQueryable<Product> lstFilteredProducts = Apps.dbContext.Product.AsQueryable();

                if (integratedFilter != null)
                {
                    switch (integratedFilter.ExecutionFrom)
                    {
                        case ExecutedFrom.Category:
                            MasterCategories selectedCategory = Apps.dbContext.MasterCategories.FirstOrDefault(c => c.ID.Equals(integratedFilter.Id));
                            if (selectedCategory == null)
                            {
                                result = Messages.CateogryMsg + " " + Messages.DoesnotExists;
                                return null;
                            }

                            if (selectedCategory.LSTPRODUCTS != null && selectedCategory.LSTPRODUCTS.Count > 0)
                            {
                                lstFilteredProducts = selectedCategory.LSTPRODUCTS.Where(c => c.RELATEDSTORE.ID.Equals(integratedFilter.DefaultStoreId) && c.ISACTIVE).AsQueryable();
                            }
                            break;
                        case ExecutedFrom.Offers:
                            MasterOffers selectedOffer = Apps.dbContext.MasterOffers.FirstOrDefault(c => c.ID.Equals(integratedFilter.Id));
                            if (selectedOffer == null)
                            {
                                result = Messages.Offer + " " + Messages.DoesnotExists;
                            }
                            if (selectedOffer.LSTPRODUCTS != null && selectedOffer.LSTPRODUCTS.Count > 0)
                            {
                                lstFilteredProducts = selectedOffer.LSTPRODUCTS.Where(c => c.RELATEDSTORE.ID.Equals(integratedFilter.DefaultStoreId) && c.ISACTIVE).AsQueryable();
                            }
                            break;
                        case ExecutedFrom.NewArrivals:
                            DateTime checkedDateTime = DateTime.Now.AddDays(-integratedFilter.Id);
                            lstProducts = Apps.dbContext.Product.Where(c => c.RELATEDSTORE != null && c.LASTUPDATEDTIME >= checkedDateTime && c.RELATEDSTORE.ID.Equals(integratedFilter.DefaultStoreId)).ToList();
                            lstProducts = lstProducts.Where(c => c.RELATEDCATEGORIES != null && c.ISACTIVE && c.RELATEDCATEGORIES.Any(d => d.ID.Equals(integratedFilter.CategoryID))).ToList();
                            lstFilteredProducts = lstProducts.AsQueryable();
                            break;
                        default:
                            break;
                    }
                }

                if (integratedFilter != null && lstFilteredProducts != null && integratedFilter.LstGroupedFilters != null)
                {
                    integratedFilter.LstGroupedFilters.Where(c => c.LstMultiFilter != null && c.LstMultiFilter.Count > 0).ToList().ForEach(c =>
                    {
                        lstFilteredProducts = ApplyFilter(lstFilteredProducts, d => d.RELATEDFILTERS != null && d.RELATEDFILTERS.Any(f => c.LstMultiFilter.Select(g => g.Id).Contains(f.ID)));
                    });

                    if (integratedFilter.StartPrice > 0)
                    {
                        lstFilteredProducts = ApplyFilter(lstFilteredProducts, d => d.PRICE >= integratedFilter.StartPrice);
                    }
                    if (integratedFilter.EndPrice > 0)
                    {
                        lstFilteredProducts = ApplyFilter(lstFilteredProducts, d => d.PRICE <= integratedFilter.EndPrice);
                    }
                }

                lstFilteredProducts = lstFilteredProducts.Where(d=>d.ISACTIVE).OrderByDescending(c=>c.LASTUPDATEDTIME).Skip((pageNumber - 1) * 20).Take(20);

                List<Product> lstMainProducts = lstFilteredProducts.AsQueryable().ToList();

                Apps.Logger.Info("Products Count: " + lstFilteredProducts.Count());

                //Transfering DTO to Model
                if (lstMainProducts != null && lstMainProducts.Count > 0)
                {
                    lstProducts = new List<Product>();
                    lstMainProducts.ForEach(c =>
                    {
                        Product prod = new Product();
                        prod = PopulateProductFields.AssignProductValues(prod, c);
                        lstProducts.Add(prod);
                    });
                }
                else
                {
                    lstProducts = lstMainProducts;
                }

                return lstProducts;
            }
            catch (Exception ex)
            {
                result = ex.Message;
                return new List<Product>();
            }
        }

        private static IQueryable<Product> ApplyFilter(IQueryable<Product> lstProducts,
                                                                          Expression<Func<Product, bool>> predicate)
        {
            return lstProducts.Where(predicate);
        }
    }
}