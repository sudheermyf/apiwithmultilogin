﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.ExpressInterests
{
    public class ExpressInterestOperationsLogic
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public ExpressInterestOperationsLogic(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
        public List<ExpressInterest> GetExpressInterestUnderStore(long storeID, out string result)
        {
            result = Messages.Succcess;
            List<ExpressInterest> lstExpressInterest = new List<ExpressInterest>();

            if (storeID > 0 && HeaderInformation.LstStoreIds.Contains(storeID))
            {
                List<ExpressInterest> lstFetchedInterests = Apps.dbContext.ExpressInterest.Where(c => c.RELATEDSTORE != null && c.RELATEDSTORE.ID.Equals(storeID)).ToList();
                lstFetchedInterests.ForEach(c =>
                    {
                        ExpressInterest interest = PopulateExpressInterest(c);
                        if (interest != null)
                        {
                            lstExpressInterest.Add(interest);
                        }
                    });
            }
            else
            {
                result = Messages.NotAuthorize;
            }
            return lstExpressInterest;
        }

        public List<ExpressInterest> GetExpressInterestUnderCompany(long companyID, out string result)
        {
            result = Messages.Succcess;
            List<ExpressInterest> lstExpressInterest = new List<ExpressInterest>();
            if (companyID > 0 && HeaderInformation.CompanyID.Equals(companyID))
            {
                List<ExpressInterest> lstFetchedInterests = Apps.dbContext.ExpressInterest.Where(c => c.RELATEDCOMPANY != null && c.RELATEDCOMPANY.ID.Equals(companyID)).ToList();
                lstFetchedInterests.ForEach(c =>
                {
                    ExpressInterest interest = PopulateExpressInterest(c);
                    if (interest != null)
                    {
                        lstExpressInterest.Add(interest);
                    }
                });
            }
            else
            {
                result = Messages.NotAuthorize;
            }
            return lstExpressInterest;
        }

        public List<ExpressInterest> GetExpressInterestUnderProduct(long productID, out string result)
        {
            result = Messages.Succcess;
            List<ExpressInterest> lstExpressInterest = new List<ExpressInterest>();
            if (productID > 0 && Apps.dbContext.Product.ToList().Exists(c=>c.ID.Equals(productID)))
            {
                Product selectedProd = Apps.dbContext.Product.FirstOrDefault(c => c.ID.Equals(productID));
                if (selectedProd.RELATEDSTORE != null && selectedProd.RELATEDSTORE.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID))
                {
                    List<ExpressInterest> lstFetchedInterests = Apps.dbContext.ExpressInterest.Where(c => c.RELATEDPRODUCT != null && c.RELATEDPRODUCT.ID.Equals(productID)).ToList();
                    lstFetchedInterests.ForEach(c =>
                    {
                        ExpressInterest interest = PopulateExpressInterest(c);
                        if (interest != null)
                        {
                            lstExpressInterest.Add(interest);
                        }
                    });
                }
                else
                {
                    result = Messages.NotAuthorize;
                }
            }
            else
            {
                result = Messages.ProductMsg + " " + Messages.DoesnotExists;
            }
            return lstExpressInterest;
        }
        public List<Product> GetExpressInterestUnderEmail(string emailAddress, out string result)
        {
            result = Messages.Succcess;
            try
            {
                List<ExpressInterest> lstExpressInterest = new List<ExpressInterest>();
                List<ExpressInterest> lstFetchedInterests = Apps.dbContext.ExpressInterest.Where(c => c.EMAIL.Equals(emailAddress)).ToList();
                lstFetchedInterests.ForEach(c =>
                    {
                        ExpressInterest interest = PopulateExpressInterest(c);
                        if (interest != null)
                        {
                            lstExpressInterest.Add(interest);
                        }
                    });
                Apps.Logger.Info("Express Interest Count: " + lstExpressInterest.Count);

                return lstExpressInterest.Select(c => c.RELATEDPRODUCT).ToList();
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("GetExpressInterestUnderEmail: " + ex.Message, ex);
            }
            return null;
        }
        public ExpressInterest GetInterestUnderEmail(string emailAddress1, bool tempParam1,out string result)
        {
            result = Messages.Succcess;
            emailAddress1 = emailAddress1.Trim();
            try
            {
                ExpressInterest selectedExpInterest = Apps.dbContext.ExpressInterest.FirstOrDefault(c => c.EMAIL.Equals(emailAddress1));
                ExpressInterest interest = PopulateExpressInterest(selectedExpInterest);

                Apps.Logger.Info("GetInterestUnderEmail Success: " + emailAddress1);

                return interest;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("GetExpressInterestUnderEmail: " + ex.Message, ex);
            }
            return null;
        }

        private ExpressInterest PopulateExpressInterest(ExpressInterest expressInterest)
        {
            ExpressInterest interest = new ExpressInterest();
            interest = PopulateInterestFields.AssignInterestValues(interest, expressInterest);
            interest.SNO = expressInterest.SNO;
            interest.ID = expressInterest.ID;
            //if (expressInterest.RELATEDSTORE != null && expressInterest.RELATEDSTORE.ID > 0)
            //{
            //    interest.RELATEDSTORE = PopulateStoreFields.AssignStoreValues(interest.RELATEDSTORE, expressInterest.RELATEDSTORE);
            //}

            //if (expressInterest.RELATEDCOMPANY != null && expressInterest.RELATEDCOMPANY.ID > 0)
            //{
            //    interest.RELATEDCOMPANY = PopulateCompanyFields.AssignCompanyValues(interest.RELATEDCOMPANY, expressInterest.RELATEDCOMPANY);
            //}

            if (expressInterest.RELATEDPRODUCT != null && expressInterest.RELATEDPRODUCT.ID > 0)
            {
                interest.RELATEDPRODUCT = PopulateProductFields.AssignProductValues(interest.RELATEDPRODUCT, expressInterest.RELATEDPRODUCT, isFromCreate: true, offersOnly: true);
            }

            return interest;
        }

        public string AddExpressInterest(ExpressInterest expressInterest)
        {
            try
            {
                Apps.Logger.Info("Initiating AddExpressInterest.");
                string result = ValidateExpressInterest(expressInterest, "A");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnExpressInterest.AddExpressInterest(expressInterest);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Add Express Interest: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteExpressInterest(long productId, string emailAddress)
        {
            try
            {
                emailAddress = emailAddress.Trim();
                Apps.Logger.Info("Initiating DeleteExpressInterest.");
                string result = ValidateDeleteExpressInterest(productId, emailAddress);
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnExpressInterest.DeleteExpressInterest(productId, emailAddress);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Delete Express Interest: " + ex.Message, ex);
                return ex.Message;
            }
        }

        private string ValidateDeleteExpressInterest(long productId, string emailAddress)
        {
            if (productId > 0 && !string.IsNullOrEmpty(emailAddress))
            {
                if (Apps.dbContext.ExpressInterest.ToList().Exists(
                        c =>
                            c.RELATEDPRODUCT.ID.Equals(productId) &&
                            c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return Messages.Succcess;
                }
                else
                {
                    return Messages.ExpressInterest + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                return Messages.RequiredFieldsEmpty;
            }
        }

        private string ValidateExpressInterest(ExpressInterest expressInterest, string parameter)
        {
            string result = string.Empty;
            switch (parameter)
            {
                case "A":
                    if ((!string.IsNullOrEmpty(expressInterest.NAME) && !string.IsNullOrEmpty(expressInterest.EMAIL))
                        && ((expressInterest.RELATEDCOMPANY != null && expressInterest.RELATEDCOMPANY.ID > 0) || (expressInterest.RELATEDSTORE != null && expressInterest.RELATEDSTORE.ID > 0))
                        && (expressInterest.RELATEDPRODUCT != null && expressInterest.RELATEDPRODUCT.ID > 0
                        && Apps.dbContext.Product.ToList().Exists(c => c.ID.Equals(expressInterest.RELATEDPRODUCT.ID) && c.RELATEDSTORE.REGISTEREDCOMPANY.ID.Equals(expressInterest.RELATEDCOMPANY.ID))))
                    {
                        if (
                            Apps.dbContext.ExpressInterest.ToList()
                                .Exists(
                                    c =>
                                        c.EMAIL.Equals(expressInterest.EMAIL,
                                            StringComparison.InvariantCultureIgnoreCase) &&
                                        c.RELATEDPRODUCT.ID.Equals(expressInterest.RELATEDPRODUCT.ID)))
                        {
                            result = Messages.AlreadyExistsExpressInterest;
                        }
                        else
                        {
                            result = Messages.Succcess;    
                        }
                    }
                    else
                    {
                        result = Messages.RequiredFieldsEmpty;
                    }
                    break;
                case "D":
                case "U":
                default:
                    result = Messages.InvalidCRUDOperation;
                    break;
            }
            return result;
        }

        public string UpdateExpressInterest(string emailAddress, string contactNumber, string Address, out string erroMessage)
        {
            erroMessage = string.Empty;
            if (!string.IsNullOrEmpty(emailAddress))
            {
                List<ExpressInterest> lstExpressInterests = Apps.dbContext.ExpressInterest.Where(c => c.EMAIL.Equals(emailAddress)).ToList();
                Apps.Logger.Info("Express Count: " + (lstExpressInterests != null ? lstExpressInterests.Count : 0));
                if (lstExpressInterests != null && lstExpressInterests.Count > 0)
                {
                    lstExpressInterests.ForEach(c =>
                        {
                            c.CONTACTNUMBER = contactNumber;
                            c.ADDRESS = Address;
                        });
                    Apps.dbContext.SaveChanges();
                    return Messages.Succcess;
                }
                else
                {
                    erroMessage = Messages.ExpressInterest + " " + Messages.NotFound;
                }
            }
            else
            {
                erroMessage = Messages.EmailAddress + " " + Messages.NotFound;
            }
            return Messages.Succcess;
        }
    }
}
