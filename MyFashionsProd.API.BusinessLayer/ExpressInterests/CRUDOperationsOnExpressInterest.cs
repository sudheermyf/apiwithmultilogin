﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.ExpressInterests
{
    public class CRUDOperationsOnExpressInterest
    {
        internal static string AddExpressInterest(ExpressInterest expressInterest)
        {
            string result = string.Empty;
            if (expressInterest != null)
            {
                try
                {
                    ExpressInterest addInterest = new ExpressInterest();
                    addInterest = PopulateInterestFields.AssignInterestValues(addInterest, expressInterest);
                    addInterest.ID = Apps.dbContext.ExpressInterest.ToList().Count > 0 ? Apps.dbContext.ExpressInterest.Max(c => c.ID) + 1 : 1;
                    addInterest.ISACTIVE = true;
                    addInterest.LASTUPDATEDTIME = DateTime.Now;

                    if (expressInterest.RELATEDCOMPANY != null && expressInterest.RELATEDCOMPANY.ID > 0)
                    {
                        addInterest.RELATEDCOMPANY = Apps.dbContext.Company.FirstOrDefault(c => c.ID.Equals(expressInterest.RELATEDCOMPANY.ID));
                    }

                    if (expressInterest.RELATEDPRODUCT != null && expressInterest.RELATEDPRODUCT.ID > 0)
                    {
                        addInterest.RELATEDPRODUCT = Apps.dbContext.Product.FirstOrDefault(c => c.ID.Equals(expressInterest.RELATEDPRODUCT.ID));
                    }

                    if (expressInterest.RELATEDSTORE != null && expressInterest.RELATEDSTORE.ID > 0)
                    {
                        addInterest.RELATEDSTORE = Apps.dbContext.Stores.First(c => c.ID.Equals(expressInterest.RELATEDSTORE.ID));
                    }

                    Apps.dbContext.ExpressInterest.Add(addInterest);
                    Apps.dbContext.SaveChanges();
                    result = Messages.Succcess;
                }
                catch (Exception ex)
                {
                    Apps.Logger.ErrorException("CRUDOperationsOnExpressInterest -> AddExpressInterest: " + ex.Message, ex);
                    result = ex.Message.ToString();
                }
            }
            else
            {
                result = Messages.ObjectShouldNotBeNull;
            }
            return result;
        }

        internal static string DeleteExpressInterest(long productId, string emailAddress)
        {
            string result = string.Empty;

            try
            {
                if (
                    Apps.dbContext.ExpressInterest.ToList().Exists(
                        c =>
                            c.RELATEDPRODUCT.ID.Equals(productId) &&
                            c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase)))
                {
                    ExpressInterest expInterest =
                        Apps.dbContext.ExpressInterest.FirstOrDefault(
                            c =>
                                c.RELATEDPRODUCT.ID.Equals(productId) &&
                                c.EMAIL.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase));
                    if (expInterest != null)
                    {
                        Apps.dbContext.ExpressInterest.Remove(expInterest);
                        Apps.dbContext.SaveChanges();
                        result = Messages.Succcess;
                    }
                }
                else
                {
                    result = Messages.ExpressInterest + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("CRUDOperationsOnExpressInterest -> DeleteExpressInterest: " + ex.Message, ex);
                result = ex.Message.ToString();
            }

            return result;
        }
    }
}
