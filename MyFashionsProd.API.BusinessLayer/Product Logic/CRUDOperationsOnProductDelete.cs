﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Product_Logic
{
    public class CRUDOperationsOnProductDelete
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public CRUDOperationsOnProductDelete(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
        internal static string DeleteProduct(Product prod)
        {
            string result = string.Empty;
            try
            {
                if (prod != null)
                {
                    if (Apps.dbContext.Product.Any(c => c.ID.Equals(prod.ID)))
                    {
                        Product masterProd = Apps.dbContext.Product.FirstOrDefault(c => c.ID.Equals(prod.ID));
                        masterProd.ISACTIVE = false;
                        masterProd.VERSION += 1;
                        masterProd.LASTUPDATEDTIME = DateTime.Now;
                        Apps.dbContext.SaveChanges();
                        result = Messages.Succcess;
                    }
                    else
                    {
                        result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                    }
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteProduct: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return result;
        }
    }
}
