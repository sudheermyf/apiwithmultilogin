﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Product_Logic
{
    public class ProductsOperationLogic
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public ProductsOperationLogic(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
        public List<Product> GetAllProductsUnderStore(long storeID, int pageNumber, out string result)
        {
            Apps.Logger.Info("Initiating GetAllProductsUnderStore. Store ID: " + storeID);
            if (pageNumber <= 0)
            {
                pageNumber = 1;
            }
            Apps.Logger.Info("Page Number: " + pageNumber);
            result = string.Empty;
            List<Product> lstProducts = new List<Product>();

            try
            {
                if (storeID > 0 && Apps.dbContext.Stores.Any(c => c.ID.Equals(storeID)))
                {
                    List<Product> lstProds = Apps.dbContext.Product.Where(c => c.RELATEDSTORE.ID.Equals(storeID)).OrderByDescending(c=>c.LASTUPDATEDTIME).Skip((pageNumber - 1) * 20).Take(20).ToList();
                    if (lstProds != null && lstProds.Count > 0)
                    {
                        lstProds.ForEach(c =>
                        {
                            Product prod = new Product();
                            prod = PopulateProductFields.AssignProductValues(prod, c);
                            lstProducts.Add(prod);
                        });
                    }
                    Apps.Logger.Info("Qualified Products Count: " + lstProducts.Count);
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.StoreMsg + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while GetAllProductsUnderStore: " + ex.Message, ex);
                result = ex.Message;
            }

            Apps.Logger.Info("Result: " + result);
            return lstProducts;
        }

        public List<Product> GetProductsByCategoryID(long categoryId, long storeId, int pageNumber, out string result)
        {
            Apps.Logger.Info("Initiating GetProductsByCategoryID. Category ID: " + categoryId);
            if (pageNumber <= 0)
            {
                pageNumber = 1;
            }
            Apps.Logger.Info("Page Number: " + pageNumber);
            result = string.Empty;
            List<Product> lstProducts = new List<Product>();
            try
            {
                if (categoryId > 0 && Apps.dbContext.MasterCategories.Any(c => c.ID.Equals(categoryId)))
                {
                    List<Product> lstProds = Apps.dbContext.MasterCategories.FirstOrDefault(c => c.ID.Equals(categoryId)).LSTPRODUCTS.Where
                        (c => c.RELATEDSTORE.ID.Equals(storeId) && c.ISACTIVE).OrderByDescending(c=>c.LASTUPDATEDTIME).Skip((pageNumber - 1) * 20).Take(20).ToList();

                    if (lstProds != null && lstProds.Count > 0)
                    {
                        lstProds.ForEach(c =>
                        {
                            Product prod = new Product();
                            prod = PopulateProductFields.AssignProductValues(prod, c);
                            lstProducts.Add(prod);
                        });
                    }
                    Apps.Logger.Info("Qualified Products Count: " + lstProducts.Count);
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.StoreMsg + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while GetAllProductsUnderStore: " + ex.Message, ex);
                result = ex.Message;
            }

            Apps.Logger.Info("Result: " + result);
            return lstProducts;
        }

        public List<Product> GetAllBasicProductInforation(long categoryId, long storeId, out string result)
        {
            Apps.Logger.Info("Initiating GetAllBasicProductInforation. Category ID: " + categoryId);
            Apps.Logger.Info("Initiating GetAllBasicProductInforation. Store ID: " + storeId);
            result = string.Empty;
            List<Product> lstProducts = new List<Product>();
            try
            {
                if (categoryId > 0 && Apps.dbContext.MasterCategories.Any(c => c.ID.Equals(categoryId)))
                {
                    List<Product> lstProds = Apps.dbContext.MasterCategories.FirstOrDefault(c => c.ID.Equals(categoryId)).LSTPRODUCTS.Where
                        (c => c.RELATEDSTORE.ID.Equals(storeId)).OrderBy(c => c.ID).ToList();

                    if (lstProds != null && lstProds.Count > 0)
                    {
                        lstProds.ForEach(c =>
                        {
                            Product prod = new Product();
                            prod = PopulateProductFields.AssignProductValues(prod, c, isFromCreate: true);
                            lstProducts.Add(prod);
                        });
                    }
                    Apps.Logger.Info("Qualified Products Count: " + lstProducts.Count);
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.StoreMsg + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while GetAllProductsUnderStore: " + ex.Message, ex);
                result = ex.Message;
            }

            Apps.Logger.Info("Result: " + result);
            return lstProducts;
        } 

        public List<Product> GetProductsUnderNewArrivals(long arrivalDays, long storeId, int pageNumber, long categoryID, out string result)
        {
            Apps.Logger.Info("Initiating GetProductsUnderNewArrivals. Arrival Days: " + arrivalDays);

            if (pageNumber <= 0)
            {
                pageNumber = 1;
            }

            Apps.Logger.Info("Page Number: " + pageNumber);
            result = string.Empty;
            List<Product> lstProducts = new List<Product>();
            try
            {
                if (arrivalDays > 0)
                {
                    DateTime checkedDateTime = DateTime.Now.AddDays(-arrivalDays);
                    List<Product> lstProds = Apps.dbContext.Product.Where(c => c.RELATEDCATEGORIES.Any(d=>d.ID.Equals(categoryID)) 
                        && c.LASTUPDATEDTIME >= checkedDateTime && c.RELATEDSTORE.ID.Equals(storeId) 
                        && c.ISACTIVE.Equals(true))
                        .OrderByDescending(c=>c.LASTUPDATEDTIME)
                        .Skip((pageNumber - 1) * 20)
                        .Take(20).ToList();

                    if (lstProds != null && lstProds.Count > 0)
                    {
                        lstProds.ForEach(c =>
                        {
                            Product prod = new Product();
                            prod = PopulateProductFields.AssignProductValues(prod, c);
                            lstProducts.Add(prod);
                        });
                    }
                    Apps.Logger.Info("Qualified Products Count: " + lstProducts.Count);
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.ArrivalDays + " " + Messages.ShouldNotBeZero;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while GetAllProductsUnderStore: " + ex.Message, ex);
                result = ex.Message;
            }
            Apps.Logger.Info("Result: " + result);
            return lstProducts;
        }

        public Product GetProductInformation(long productID, out string result)
        {
            Apps.Logger.Info("Initiating GetProductInformation. Product ID: " + productID);
            result = string.Empty;
            Product productInfo = new Product();

            try
            {
                if (Apps.dbContext.Product.ToList().Exists(c => c.ID.Equals(productID) && ((HeaderInformation.Login.Equals(LoginType.SuperAdmin.ToString())) ||
                    (c.RELATEDSTORE != null && c.RELATEDSTORE.REGISTEREDCOMPANY != null && c.RELATEDSTORE.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID)))))
                {
                    string strLogintype = LoginType.SuperAdmin.ToString();
                    long compID = Convert.ToInt64(HeaderInformation.CompanyID);
                    
                    Product fetchedProduct = Apps.dbContext.Product.FirstOrDefault(c => c.ID.Equals(productID) && c.RELATEDSTORE.REGISTEREDCOMPANY.ID.Equals(compID));
                    if (fetchedProduct != null)
                    {
                        Apps.Logger.Info("Qualified Product Name: " + fetchedProduct.NAME);
                        productInfo = PopulateProductFields.AssignProductValues(productInfo, fetchedProduct);
                        result = Messages.Succcess;
                    }
                    else
                    {
                        result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                    }
                }
                else
                {
                    result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                }
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while GetProductInformation: " + ex.Message, ex);
                result = ex.Message;
            }

            Apps.Logger.Info("Result: " + result);
            return productInfo;
        }

        public string AddProduct(Product prod)
        {
            try
            {
                Apps.Logger.Info("Initiating AddProduct.");
                string result = ValidateMasterProduct(prod, "A");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnProductAdd.AddProduct(prod);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddProduct: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string UpdateProduct(Product prod)
        {
            try
            {
                Apps.Logger.Info("Initiating UpdateProduct.");
                string result = ValidateMasterProduct(prod, "U");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnProductUpdate.UpdateProduct(prod);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while UpdateProduct: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteProduct(Product prod)
        {
            try
            {
                Apps.Logger.Info("Initiating DeleteProduct.");
                string result = ValidateMasterProduct(prod, "D");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnProductDelete.DeleteProduct(prod);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteProduct: " + ex.Message, ex);
                return ex.Message;
            }
        }

        private string ValidateMasterProduct(Product prod, string parameter)
        {
            string result = string.Empty;
            switch (parameter)
            {
                case "A":
                    if (prod != null && !string.IsNullOrEmpty(prod.IMAGENAME) && prod.LSTPRODUCTIMAGES != null && prod.LSTPRODUCTIMAGES.Count > 0
                        && !string.IsNullOrEmpty(prod.NAME) && !string.IsNullOrEmpty(prod.PRODUCTTYPE) && prod.RELATEDCATEGORIES != null && prod.RELATEDCATEGORIES.Count > 0
                        && prod.RELATEDFILTERS != null && prod.RELATEDFILTERS.Count > 0 && prod.RELATEDSTORE != null && prod.RELATEDSTORE.ID > 0)
                    {
                        if (!Apps.dbContext.Product.Any(c => c.ID.Equals(prod.ID) && c.IMAGENAME.Equals(prod.IMAGENAME)))
                        {
                            result = ValidateProduct(prod, result);
                        }
                        else
                        {
                            result = Messages.ProductMsg + " " + Messages.AlreadyExists;
                        }
                    }
                    else
                    {
                        result = Messages.RequiredFieldsEmpty;
                    }
                    break;
                case "U":
                     if (prod != null && !string.IsNullOrEmpty(prod.IMAGENAME) && prod.LSTPRODUCTIMAGES != null && prod.LSTPRODUCTIMAGES.Count > 0
                        && !string.IsNullOrEmpty(prod.NAME) && !string.IsNullOrEmpty(prod.PRODUCTTYPE) && prod.RELATEDCATEGORIES != null && prod.RELATEDCATEGORIES.Count > 0
                        && prod.RELATEDFILTERS != null && prod.RELATEDFILTERS.Count > 0 && prod.RELATEDSTORE != null && prod.RELATEDSTORE.ID > 0)
                    {
                        if (Apps.dbContext.Product.Any(c => c.ID.Equals(prod.ID)))
                        {
                            result = ValidateProduct(prod, result);
                        }
                        else
                        {
                            result = Messages.ProductMsg + " " + Messages.AlreadyExists;
                        }
                    }
                    else
                    {
                        result = Messages.RequiredFieldsEmpty;
                    }
                    break;
                case "D":
                    if (prod != null && !string.IsNullOrEmpty(prod.IMAGENAME) && prod.ID > 0 && prod.SNO > 0 && prod.LSTPRODUCTIMAGES != null && prod.LSTPRODUCTIMAGES.Count > 0
                        && !string.IsNullOrEmpty(prod.NAME) && !string.IsNullOrEmpty(prod.PRODUCTTYPE) && prod.RELATEDCATEGORIES != null && prod.RELATEDCATEGORIES.Count > 0
                        && prod.RELATEDFILTERS != null && prod.RELATEDFILTERS.Count > 0 && prod.RELATEDSTORE != null && prod.RELATEDSTORE.ID > 0)
                    {
                        if (Apps.dbContext.Product.Any(c => c.ID.Equals(prod.ID) && c.NAME.Equals(prod.NAME)))
                        {
                            result = ValidateProduct(prod, result);
                        }
                        else
                        {
                            result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                        }
                    }
                    else
                    {
                        result = Messages.RequiredFieldsEmpty;
                    }
                    break;
                default:
                    result = Messages.InvalidCRUDOperation;
                    break;
            }
            return result;
        }

        private static string ValidateProduct(Product prod, string result)
        {
            if (Apps.dbContext.Stores.Any(c => c.ID.Equals(prod.RELATEDSTORE.ID)))
            {
                foreach (MasterCategories masterCateg in prod.RELATEDCATEGORIES)
                {
                    if (!Apps.dbContext.MasterCategories.Any(c => c.ID.Equals(masterCateg.ID)))
                    {
                        result = Messages.CateogryMsg + " " + Messages.DoesnotExists;
                        break;
                    }
                }

                foreach (MasterFilters masterFilter in prod.RELATEDFILTERS)
                {
                    if (!Apps.dbContext.MasterFilters.Any(c => c.ID.Equals(masterFilter.ID)))
                    {
                        result = Messages.Filter + " " + Messages.DoesnotExists;
                        break;
                    }
                }

                if (string.IsNullOrEmpty(result))
                    result = Messages.Succcess;
            }
            else
            {
                result = Messages.StoreMsg + " " + Messages.DoesnotExists;
            }

            return result;
        }

        public List<ProductImageCollection> GetAllProductImages(out string result)
        {
            result = string.Empty;
            List<ProductImageCollection> lstProdImgCollection = new List<ProductImageCollection>();
            try
            {
                Apps.dbContext.ProductImageCollection.ToList().ForEach(c =>
                        {
                            
                            ProductImageCollection prodImg = new ProductImageCollection();
                            prodImg = AssignValues.PopulateProductImageCollectionFields.AssignProductImageCollectionValues(prodImg, c);
                            
                            if (prodImg.PRODUCT == null)
                            {
                                prodImg.PRODUCT = new Product();
                            }
                            prodImg.PRODUCT.ID = c.PRODUCT.ID;
                            prodImg.PRODUCT.NAME = c.PRODUCT.NAME;

                            lstProdImgCollection.Add(prodImg);
                        });
                result = Messages.Succcess;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteProduct: " + ex.Message, ex);
                result = ex.Message;
            }
            return lstProdImgCollection;
        }
    }
}
