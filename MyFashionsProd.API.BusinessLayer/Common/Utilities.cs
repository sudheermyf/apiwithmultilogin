﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Common
{
    public class Utilities
    {
        public static LoginType GetLoginType(LoginInfoCache headerInfo)
        {
            return headerInfo.Login;
        }

        public static long GetCompanyID(LoginInfoCache headerInfo)
        {
            return headerInfo.CompanyID;
        }
    }
}
