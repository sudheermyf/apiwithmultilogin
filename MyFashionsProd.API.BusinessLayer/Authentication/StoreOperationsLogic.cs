﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using MyFashionsProd.API.BusinessLayer.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Authentication
{
    public class StoreOperationsLogic
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public StoreOperationsLogic(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
        #region Validate Store Login
        public string ValidateStoreLogin(string username, string password, out long storeID)
        {
            username = username.ToUpper();
            password = password.ToUpper();
            string encryptedPassword = EncryptAndDecrypt.Encrypt(password);
            Apps.Logger.Info("Initiated Validate Store Login: UserName: " + username + " Password: " + password + " Encrypted Password: " + encryptedPassword);
            string result = string.Empty;
            storeID = 0;
            Stores validateStore = Apps.dbContext.Stores.FirstOrDefault(c => c.USERNAME.Equals(username) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID) && c.PASSWORD.Equals(encryptedPassword) && c.ISACTIVE.Equals(true));
            if (validateStore == null)
            {
                if (!Apps.dbContext.Stores.ToList().Exists(c => c.USERNAME.Equals(username)))
                    result = Messages.InvalidUserName;
                else if (!Apps.dbContext.Stores.ToList().Exists(c => c.USERNAME.Equals(username) && c.PASSWORD.Equals(encryptedPassword)))
                    result = Messages.InvalidPassword;
                else
                    result = Messages.StoreIDLoginDoesNotMatch;
            }
            else
            {
                storeID = validateStore.ID;
                result = Messages.Succcess;
            }
            Apps.Logger.Info("Result: " + result + " Store ID: " + storeID);
            return result;
        }
        #endregion

        #region Password Change
        public bool ChangePassword(string username, string oldpassword, string newpassword)
        {
            try
            {
                oldpassword = oldpassword.ToUpper();
                newpassword = newpassword.ToUpper();
                string encryptedOldPassword = EncryptAndDecrypt.Encrypt(oldpassword);
                string encryptedNewPassword = EncryptAndDecrypt.Encrypt(newpassword);

                Apps.Logger.Info("Initiated ChangePassword for Store.");
                Stores storeObj = Apps.dbContext.Stores.FirstOrDefault(c => c.USERNAME.Equals(username) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID) && c.PASSWORD.Equals(encryptedOldPassword) && c.ISACTIVE.Equals(true));
                if (storeObj != null)
                {
                    storeObj.PASSWORD = encryptedNewPassword;
                    storeObj.VERSION += 1;
                    Apps.dbContext.SaveChanges();
                    Apps.Logger.Info(Messages.Succcess);
                    return true;
                }
                Apps.Logger.Info(Messages.UnableToFind + " " + Messages.StoreMsg);
                return false;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Store ChangePassword: " + ex.Message, ex);
                return false;
            }
        }
        #endregion

        #region Fetch All Stores
        public List<Stores> GetAllStores(long relatedCompanyID)
        {
            Apps.Logger.Info("Initiated GetAllStores. Related Company ID: " + relatedCompanyID);
            List<Stores> lstStores = new List<Stores>();
            if (Apps.dbContext.Stores.ToList().Count > 0)
            {
                Apps.dbContext.Company.Where(c => c.ID.Equals(relatedCompanyID) && c.ID.Equals(HeaderInformation.CompanyID)).ToList().ForEach(c =>
                {
                    c.LSTSTORES.ToList().ForEach(d =>
                    {
                        Stores storeObj = new Stores();
                        storeObj = PopulateStoreFields.AssignStoreValues(storeObj, d);
                        if (storeObj.REGISTEREDCOMPANY == null)
                            storeObj.REGISTEREDCOMPANY = new Company();
                        storeObj.REGISTEREDCOMPANY = PopulateCompanyFields.AssignCompanyValues(storeObj.REGISTEREDCOMPANY, d.REGISTEREDCOMPANY);
                        storeObj.SNO = d.SNO;
                        lstStores.Add(storeObj);
                    });
                });
            }
            Apps.Logger.Info(Messages.Succcess);
            return lstStores;
        }

        public Stores GetStoreInfo(long storeID, out string errorMsg)
        {
            Apps.Logger.Info("Initiated GetStoreInfo. Store ID: " + storeID);
            errorMsg = string.Empty;
            if (storeID > 0)
            {
                Stores storeInfo = Apps.dbContext.Stores.Where(c => c.ID.Equals(storeID) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID)).FirstOrDefault();

                if (storeInfo != null)
                {
                    Stores storeObj = new Stores();
                    storeObj = PopulateStoreFields.AssignStoreValues(storeObj, storeInfo);
                    if (storeObj.REGISTEREDCOMPANY == null)
                        storeObj.REGISTEREDCOMPANY = new Company();
                    storeObj.REGISTEREDCOMPANY = PopulateCompanyFields.AssignCompanyValues(storeObj.REGISTEREDCOMPANY, storeInfo.REGISTEREDCOMPANY);
                    storeObj.SNO = storeInfo.SNO;
                    Apps.Logger.Info(Messages.Succcess);
                    return storeObj;
                }
                else
                {
                    errorMsg = Messages.StoreMsg + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                errorMsg = Messages.StoreMsg + " " + Messages.DoesnotExists;
            }
            Apps.Logger.Info("Error Message: " + errorMsg);
            return null;
        }
        #endregion

        #region Adding Store
        public string AddStore(Stores storeObj)
        {
            try
            {
                Apps.Logger.Info("Initiated AddStore.");
                string result = ValidateStore(storeObj, "A");
                if (result.Equals(Messages.Succcess))
                {
                    storeObj.USERNAME = storeObj.USERNAME.ToUpper();
                    storeObj.PASSWORD = EncryptAndDecrypt.Encrypt(storeObj.PASSWORD.ToUpper());

                    Stores mainStoreObj = new Stores();
                    mainStoreObj = PopulateStoreFields.AssignStoreValues(mainStoreObj, storeObj);
                    mainStoreObj.ID = Apps.dbContext.Stores.ToList().Count > 0 ? (Apps.dbContext.Stores.Max(c => c.ID) + 1) : 1;
                    mainStoreObj.REGISTEREDCOMPANY = Apps.dbContext.Company.FirstOrDefault(c => c.ID.Equals(storeObj.REGISTEREDCOMPANY.ID) && c.ISACTIVE.Equals(true));
                    mainStoreObj.VERSION = 1;
                    Apps.dbContext.Stores.Add(mainStoreObj);
                    Apps.dbContext.SaveChanges();
                    Apps.Logger.Info("Store ID: " + mainStoreObj.ID);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddStore: " + ex.Message, ex);
                return ex.Message;
            }
        }
        #endregion

        #region Updating Store
        public string UpdateStore(Stores storeObj)
        {
            try
            {
                Apps.Logger.Info("Initiating UpdateStore");
                string result = ValidateStore(storeObj, "U");
                if (result.Equals(Messages.Succcess))
                {
                    Stores mainStoreObj = Apps.dbContext.Stores.FirstOrDefault(c => c.USERNAME.Equals(storeObj.USERNAME) && c.ID.Equals(storeObj.ID) && c.REGISTEREDCOMPANY.ID.Equals(storeObj.REGISTEREDCOMPANY.ID));
                    if (mainStoreObj != null)
                    {
                        if (!mainStoreObj.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID))
                            return Messages.CompanyIDLoginDoesNotMatch;

                        storeObj.USERNAME = mainStoreObj.USERNAME;
                        storeObj.PASSWORD = mainStoreObj.PASSWORD;
                        int currentVersion = mainStoreObj.VERSION;
                        mainStoreObj = PopulateStoreFields.AssignStoreValues(mainStoreObj, storeObj);
                        mainStoreObj.VERSION = currentVersion + 1;
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Store ID: " + mainStoreObj.ID);
                    }
                    else
                    {
                        result = Messages.UnableToFind + " " + Messages.StoreMsg;
                    }
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while updatestore: " + ex.Message, ex);
                return ex.Message;
            }
        }
        #endregion

        #region Deleting Store
        public string DeleteStore(Stores storeObj)
        {
            try
            {
                Apps.Logger.Info("Initiating DeleteStore.");
                string result = ValidateStore(storeObj, "D");
                if (result.Equals(Messages.Succcess))
                {
                    Stores mainStoreObj = Apps.dbContext.Stores.FirstOrDefault(c => c.ID.Equals(storeObj.ID) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID) && c.ISACTIVE.Equals(true));
                    if (mainStoreObj != null)
                    {
                        mainStoreObj.ISACTIVE = false;
                        mainStoreObj.VERSION += 1;
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Store ID: " + mainStoreObj.ID);
                    }
                    else
                    {
                        result = Messages.UnableToFind + " " + Messages.StoreMsg;
                    }
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while deletestore: " + ex.Message, ex);
                return ex.Message;
            }
        }
        #endregion

        #region Get Company Information By Store ID
        public Company GetCompanyInfoByStoreID(long storeID, out string errorMessage)
        {
            Apps.Logger.Info("Initiated GetCompanyInfoByStoreID.");
            errorMessage = string.Empty;
            Company companyObj = new Company();
            if (storeID > 0 && Apps.dbContext.Stores.ToList().Exists(c => c.ID.Equals(storeID) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID)))
            {
                Stores fetchedStore = Apps.dbContext.Stores.Where(c => c.ID.Equals(storeID) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID)).FirstOrDefault();
                if (fetchedStore != null)
                {
                    companyObj = PopulateCompanyFields.AssignCompanyValues(companyObj, fetchedStore.REGISTEREDCOMPANY);

                }
                else
                {
                    errorMessage = Messages.StoreMsg + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                errorMessage = Messages.StoreMsg + " " + Messages.DoesnotExists;
            }
            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info(Messages.Succcess);
            return companyObj;
        }
        #endregion

        #region Validate Store Object
        private string ValidateStore(Stores storeObj, string code)
        {
            switch (code)
            {
                case "A":
                    #region Adding Validation

                    if (storeObj != null && !string.IsNullOrEmpty(storeObj.NAME) && !string.IsNullOrEmpty(storeObj.USERNAME) && !string.IsNullOrEmpty(storeObj.PASSWORD) && !string.IsNullOrEmpty(storeObj.IMAGELOCATION))
                    {
                        if (!Apps.dbContext.Stores.ToList().Exists(c => c.NAME.Equals(storeObj.NAME) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID) && c.ISACTIVE.Equals(true)))
                        {
                            if (!Apps.dbContext.Stores.ToList().Exists(c => c.USERNAME.Equals(storeObj.USERNAME) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID) && c.ISACTIVE.Equals(true)))
                            {
                                if (Apps.dbContext.Company.ToList().Exists(c => c.ID.Equals(storeObj.REGISTEREDCOMPANY.ID) && c.ISACTIVE.Equals(true) && c.USERNAME.Equals(storeObj.REGISTEREDCOMPANY.USERNAME)))
                                    return Messages.Succcess;
                                else
                                    return storeObj.REGISTEREDCOMPANY.USERNAME + " " + Messages.DoesnotExists;
                            }
                            else
                                return storeObj.USERNAME + " " + Messages.AlreadyExists;
                        }
                        else
                        {
                            return storeObj.NAME + " " + Messages.AlreadyExists;
                        }
                    }
                    else
                    {
                        return Messages.RequiredFieldsEmpty;
                    }

                    #endregion
                case "U":
                    #region Updation Validation

                    if (storeObj != null && !string.IsNullOrEmpty(storeObj.NAME) && !string.IsNullOrEmpty(storeObj.USERNAME) && !string.IsNullOrEmpty(storeObj.PASSWORD) && !string.IsNullOrEmpty(storeObj.IMAGELOCATION))
                    {
                        if (Apps.dbContext.Stores.ToList().Exists(c => c.USERNAME.Equals(storeObj.USERNAME) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID) && c.ID.Equals(storeObj.ID)))
                        {
                            return Messages.Succcess;
                        }
                        else
                        {
                            return storeObj.USERNAME + " " + Messages.AlreadyExists;
                        }
                    }
                    else
                    {
                        return Messages.RequiredFieldsEmpty;
                    }
                    #endregion
                case "D":
                    #region Deletion Validation
                    if (storeObj != null && Apps.dbContext.Stores.ToList().Exists(c => c.USERNAME.Equals(storeObj.USERNAME) && c.ISACTIVE.Equals(true) && c.NAME.Equals(storeObj.NAME) && c.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID) && c.ID.Equals(storeObj.ID)))
                    {
                        return Messages.Succcess;
                    }
                    else
                    {
                        return Messages.InvalidObject;
                    }
                    #endregion
                default:
                    return Messages.UnKnown;
            }
        }
        #endregion
    }
}
