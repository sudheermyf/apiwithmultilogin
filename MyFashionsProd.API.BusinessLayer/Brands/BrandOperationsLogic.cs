﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Brands
{
    public class BrandOperationsLogic
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public BrandOperationsLogic(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
        public void ValidateGetAllBrands(long id, bool checkStore, out string result)
        {
            result = string.Empty;
            if (checkStore)
            {
                if (Apps.dbContext.Stores.ToList().Exists(c => c.ID.Equals(id) && HeaderInformation.LstStoreIds.Contains(c.ID)))
                {
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.StoreMsg + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                if (Apps.dbContext.Company.ToList().Exists(c => c.ID.Equals(id) && c.ID.Equals(HeaderInformation.CompanyID)))
                {
                    result = Messages.Succcess;
                }
                else
                {
                    result = Messages.Company + " " + Messages.DoesnotExists;
                }
            }
        }

        public List<Brand> GetAllBrands(long id, bool checkStore,out string result)
        {
            List<Brand> lstBrands = new List<Brand>();
            result = string.Empty;
            ValidateGetAllBrands(id, checkStore, out result);
            if(checkStore)
            {
                if(result.Equals(Messages.Succcess))
                {
                    Stores fetchedStore = Apps.dbContext.Stores.FirstOrDefault(c => c.ID.Equals(id));
                    if (fetchedStore != null)
                    {
                        List<Brand> lstFetcchedBrands = Apps.dbContext.Brands.Where(c => c.SELECTEDCOMPANY.ID.Equals(fetchedStore.REGISTEREDCOMPANY.ID)).ToList();
                        if (lstFetcchedBrands != null && lstFetcchedBrands.Count > 0)
                        {
                            lstFetcchedBrands.ForEach(c =>
                                {
                                    Brand mainBrand = new Brand();
                                    mainBrand = PopulateBrandFields.AssignBrandValues(mainBrand, c);
                                    lstBrands.Add(mainBrand);
                                });
                        }
                    }
                    else
                    {
                        result = Messages.StoreMsg + " " + Messages.DoesnotExists;
                    }
                }
            }
            else
            {
                if (result.Equals(Messages.Succcess))
                {
                    List<Brand> lstFetcchedBrands = Apps.dbContext.Brands.Where(c => c.SELECTEDCOMPANY.ID.Equals(id)).ToList();
                    if (lstFetcchedBrands != null && lstFetcchedBrands.Count > 0)
                    {
                        lstFetcchedBrands.ForEach(c =>
                        {
                            Brand mainBrand = new Brand();
                            mainBrand = PopulateBrandFields.AssignBrandValues(mainBrand, c);
                            lstBrands.Add(mainBrand);
                        });
                    }
                }
            }
            return lstBrands;
        }

        public string AddBrand(Brand brandObj)
        {
            try
            {
                Apps.Logger.Info("Initiated Add Brand.");
                string result = ValidateBrand(brandObj, "A");
                if (result.Equals(Messages.Succcess))
                {
                    Brand newBrand = new Brand();
                    newBrand.ID = Apps.dbContext.Brands.ToList().Count > 0 ? Apps.dbContext.Brands.Max(c => c.ID) + 1 : 1;
                    newBrand.BRANDNAME = brandObj.BRANDNAME;
                    newBrand.DESCRIPTION = brandObj.DESCRIPTION;
                    newBrand.IMAGELOCATION = brandObj.IMAGELOCATION;
                    newBrand.IMAGENAME = brandObj.IMAGENAME;
                    newBrand.ISACTIVE = true;
                    newBrand.LASTUPDATEDTIME = DateTime.Now;
                    newBrand.SELECTEDCOMPANY = Apps.dbContext.Company.FirstOrDefault(c => c.ID.Equals(brandObj.SELECTEDCOMPANY.ID));
                    newBrand.VERSION = 1;
                    newBrand.WEBIMAGELOCATION = brandObj.WEBIMAGELOCATION;

                    newBrand = FillCategories(newBrand, brandObj);

                    Apps.dbContext.Brands.Add(newBrand);
                    Apps.dbContext.SaveChanges();
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Add Brand: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string UpdateBrand(Brand brandObj)
        {
            try
            {
                Apps.Logger.Info("Initiated Update Brand.");
                string result = ValidateBrand(brandObj, "U");
                if (result.Equals(Messages.Succcess))
                {
                    Brand updateBrand = Apps.dbContext.Brands.FirstOrDefault(c => c.ID.Equals(brandObj.ID));
                    if (updateBrand != null)
                    {
                        updateBrand.BRANDNAME = brandObj.BRANDNAME;
                        updateBrand.DESCRIPTION = brandObj.DESCRIPTION;
                        updateBrand.IMAGELOCATION = brandObj.IMAGELOCATION;
                        updateBrand.IMAGENAME = brandObj.IMAGENAME;
                        updateBrand.ISACTIVE = true;
                        updateBrand.LASTUPDATEDTIME = DateTime.Now;
                        updateBrand.VERSION = updateBrand.VERSION + 1;
                        updateBrand.WEBIMAGELOCATION = brandObj.WEBIMAGELOCATION;

                        updateBrand = FillCategories(updateBrand, brandObj);

                        Apps.dbContext.SaveChanges();
                    }
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Update Brand: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteBrand(Brand brandObj)
        {
            try
            {
                Apps.Logger.Info("Initiated Delete Brand.");
                string result = ValidateBrand(brandObj, "D");
                if (result.Equals(Messages.Succcess))
                {
                    Brand deleteBrand = Apps.dbContext.Brands.FirstOrDefault(c => c.ID.Equals(brandObj.ID));
                    if (deleteBrand != null)
                    {
                        deleteBrand.VERSION += 1;
                        deleteBrand.ISACTIVE = false;
                        deleteBrand.LASTUPDATEDTIME = DateTime.Now;
                        Apps.dbContext.SaveChanges();
                    }
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Delete Brand: " + ex.Message, ex);
                return ex.Message;
            }
        }

        private Brand FillCategories(Brand newBrand, Brand brandObj)
        {
            if (newBrand.LSTMASTERCATEGORIES != null)
            {
                newBrand.LSTMASTERCATEGORIES = new List<MasterCategories>();
            }

            #region Delete Master Categories
            if (newBrand.LSTMASTERCATEGORIES != null && newBrand.LSTMASTERCATEGORIES.Count > 0)
            {
                List<MasterCategories> lstMasterCategories = newBrand.LSTMASTERCATEGORIES.Where(c => !brandObj.LSTMASTERCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                lstMasterCategories.ForEach(c =>
                {
                    newBrand.LSTMASTERCATEGORIES.Remove(c);
                });
            }
            #endregion

            #region Add Master Categories
            if (brandObj.LSTMASTERCATEGORIES != null && brandObj.LSTMASTERCATEGORIES.Count > 0)
            {
                List<MasterCategories> lstMasterCategories = Apps.dbContext.MasterCategories.Where(c => brandObj.LSTMASTERCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                lstMasterCategories = lstMasterCategories.Where(c => !newBrand.LSTMASTERCATEGORIES.Select(d => d.ID).Contains(c.ID)).ToList();
                if (lstMasterCategories != null && lstMasterCategories.Count > 0)
                {
                    newBrand.LSTMASTERCATEGORIES.ToList().AddRange(lstMasterCategories);
                }
            }
            #endregion

            return newBrand;
        }

        private string ValidateBrand(Brand brandObj,string code)
        {
            switch (code)
            {
                case "A":
                #region Adding Validation

                    if (!string.IsNullOrEmpty(brandObj.BRANDNAME) && brandObj.SELECTEDCOMPANY != null)
                    {
                        if (Apps.dbContext.Brands.ToList().Exists(c => c.BRANDNAME.Equals(brandObj.BRANDNAME) && c.SELECTEDCOMPANY.ID.Equals(HeaderInformation.CompanyID)))
                        {
                            return Messages.Brand + " " + Messages.AlreadyExists;
                        }
                        else
                        {
                            return Messages.Succcess;
                        }
                    }
                    else
                    {
                        return Messages.RequiredFieldsEmpty;
                    }

                #endregion
                case "U":
                    if (!string.IsNullOrEmpty(brandObj.BRANDNAME) && brandObj.SELECTEDCOMPANY != null && brandObj.ID > 0 && brandObj.SNO > 0)
                    {
                        if (Apps.dbContext.Brands.ToList().Exists(c => c.ID.Equals(brandObj.ID) && c.SELECTEDCOMPANY.ID.Equals(HeaderInformation.CompanyID)))
                        {
                            return Messages.Succcess;
                        }
                        else
                        {
                            return Messages.Brand + " " + Messages.DoesnotExists;
                        }
                    }
                    else
                    {
                        return Messages.InvalidObject;
                    }
                case "D":
                    if (brandObj.SELECTEDCOMPANY != null && brandObj.ID > 0 && brandObj.SNO > 0)
                    {
                        if (Apps.dbContext.Brands.ToList().Exists(c => c.ID.Equals(brandObj.ID) && c.SELECTEDCOMPANY.ID.Equals(HeaderInformation.CompanyID)))
                        {
                            return Messages.Succcess;
                        }
                        else
                        {
                            return Messages.Brand + " " + Messages.DoesnotExists;
                        }
                    }
                    else
                    {
                        return Messages.InvalidObject;
                    }
                default:
                    return string.Empty;
            }
        }
    }
}
