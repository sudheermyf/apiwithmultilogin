﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Similar_Products_Logic
{
    public class SimilarProductOperationsLogic
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public SimilarProductOperationsLogic(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
        public List<SimilarProduct> GetAllSimilarProductUnderProduct(long productID,out string result)
        {
            result = string.Empty;
            Apps.Logger.Info("Initiating GetAllSimilarProductUnderProduct. Related Product ID: " + productID);
            List<SimilarProduct> lstSimilarProducts = new List<SimilarProduct>();
            if (productID > 0)
            {
                Product prod = Apps.dbContext.Product.Where(c => c.ID.Equals(productID)).FirstOrDefault();
                if (prod != null)
                {
                    if (prod.LSTSIMILARPRODUCTS != null)
                    {
                        prod.LSTSIMILARPRODUCTS.ToList().ForEach(c =>
                        {
                            SimilarProduct similarProd = new SimilarProduct();
                            similarProd = PopulateSimilarProductFields.AssignSimilarProductValues(similarProd, c);
                            lstSimilarProducts.Add(similarProd);
                        });
                    }
                    else
                    {
                        result = Messages.SimilarProduct + " " + Messages.DoesnotExists;
                        Apps.Logger.Info(Messages.SimilarProduct + " " + Messages.DoesnotExists);
                    }
                }
                else
                {
                    result = Messages.ProductMsg + " " + Messages.DoesnotExists;
                    Apps.Logger.Info(Messages.ProductMsg + " " + Messages.DoesnotExists);
                }
            }
            Apps.Logger.Info("Qualified Similar Products Count: " + lstSimilarProducts.Count);

            if (string.IsNullOrEmpty(result))
                result = Messages.Succcess;
            
            return lstSimilarProducts;
        }

        public string AddOrupdateSimilarProduct(Product prod)
        {
            try
            {
                Apps.Logger.Info("Initiating AddOrupdateSimilarProduct");
                string result = string.Empty;
                if (prod != null && prod.ID > 0 && !string.IsNullOrEmpty(prod.NAME))
                {
                    result = CRUDOperationsOnSimilarProduct.AddOrUpdateSimilarProduct(prod);
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while AddOrupdateSimilarProduct: " + ex.Message, ex);
                return ex.Message;
            }
        }


        public string DeleteSimilarProduct(long similarProductID)
        {
            try
            {
                Apps.Logger.Info("Initiating Delete Similar Product");
                string result = string.Empty;
                if (similarProductID > 0)
                {
                    result = new CRUDOperationsOnSimilarProduct(HeaderInformation).DeleteSimilarProduct(similarProductID);
                }
                else
                {
                    result = Messages.ObjectShouldNotBeNull;
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteSimilarProduct: " + ex.Message, ex);
                return ex.Message;
            }
        }
    }
}
