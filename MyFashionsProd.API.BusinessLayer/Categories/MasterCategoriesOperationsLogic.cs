﻿using MyfashionsDB.DBEnums;
using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Categories
{
    public class MasterCategoriesOperationsLogic
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public MasterCategoriesOperationsLogic(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
 
        #region Fetch All Main Categories under Main Enum
        public MainEnum GetAllMainCategories(long relatedCompanyID, out string errorMessage)
        {
            Apps.Logger.Info("Inititaing GetAllMainCategories. Related Company ID: " + relatedCompanyID);
            MainEnum mainCategories = new MainEnum();
            errorMessage = string.Empty;
            if (relatedCompanyID > 0)
            {
                if (HeaderInformation.CompanyID.Equals(relatedCompanyID))
                {
                    if (Apps.dbContext.MainEnum.ToList().Exists(c => c.NAME.Equals(TopEnums.Categories.ToString())))
                    {
                        MainEnum fetchedMasterMainCategory = Apps.dbContext.MainEnum.Where(c => c.NAME.Equals(TopEnums.Categories.ToString())).FirstOrDefault();
                        if (fetchedMasterMainCategory != null)
                        {
                            #region Assign Values
                            mainCategories.ID = fetchedMasterMainCategory.ID;
                            mainCategories.IMAGELOCATION = fetchedMasterMainCategory.IMAGELOCATION;
                            mainCategories.IMAGENAME = fetchedMasterMainCategory.IMAGENAME;
                            mainCategories.ISACTIVE = fetchedMasterMainCategory.ISACTIVE;
                            mainCategories.LASTUPDATEDTIME = fetchedMasterMainCategory.LASTUPDATEDTIME;
                            mainCategories.NAME = fetchedMasterMainCategory.NAME;
                            mainCategories.SNO = fetchedMasterMainCategory.SNO;
                            mainCategories.VERSION = fetchedMasterMainCategory.VERSION;
                            mainCategories.WEBIMAGELOCATION = fetchedMasterMainCategory.WEBIMAGELOCATION;
                            if (fetchedMasterMainCategory.LSTCATEGORIES != null)
                            {
                                fetchedMasterMainCategory.LSTCATEGORIES.Where(d => d.REGCOMPANY.ID.Equals(HeaderInformation.CompanyID)).ToList().ForEach(d =>
                                    {
                                        MasterCategories mainSubCategory = new MasterCategories();
                                        mainSubCategory = PopulateCategoryFields.AssignSubCategoryValues(mainSubCategory, d, fetchFilters: false, fetchProducts: false);
                                        if (mainCategories.LSTCATEGORIES == null)
                                            mainCategories.LSTCATEGORIES = new List<MasterCategories>();
                                        mainCategories.LSTCATEGORIES.Add(mainSubCategory);
                                    });
                            }
                            #endregion
                        }
                        else
                        {
                            errorMessage = Messages.UnableToFind + " " + Messages.CateogryMsg;
                        }
                    }
                    else
                    {
                        errorMessage = Messages.UnableToFind + " " + Messages.CateogryMsg + " " + Messages.RunInitiateSeed;
                    }
                }
                else
                {
                    errorMessage = Messages.NotAuthorize;
                }
            }
            else
            {
                errorMessage = Messages.CateogryMsg + " " + Messages.DoesnotExists;
            }
            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);

            return mainCategories;
        } 
        #endregion

        #region Fetch Individual SubCategory
        public MasterCategories GetSingleCategory(long relatedCategoryID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetSingleCategory. Related Category ID: " + relatedCategoryID);
            errorMessage = string.Empty;
            MasterCategories mainMasterCateg = new MasterCategories();
            if (relatedCategoryID > 0)
            {
                MasterCategories fetchedMasterCateg = Apps.dbContext.MasterCategories.Where(c => c.ID.Equals(relatedCategoryID)).FirstOrDefault();
                if (fetchedMasterCateg.REGCOMPANY.ID.Equals(HeaderInformation.CompanyID))
                {
                    if (fetchedMasterCateg != null)
                        mainMasterCateg = PopulateCategoryFields.AssignSubCategoryValues(mainMasterCateg, fetchedMasterCateg);
                    else
                        errorMessage = Messages.SubCateogryMsg + " " + Messages.DoesnotExists;
                }
                else
                {
                    errorMessage = Messages.NotAuthorize;
                }
            }
            else
            {
                errorMessage = Messages.SubCateogryMsg + " " + Messages.DoesnotExists;
            }

            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);
            return mainMasterCateg;
        }
        #endregion

        #region Fetch All Sub Categories By its Category ID
        public List<MasterCategories> GetSubCategories(long relatedMasterCategoryID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetSubCategories. Related Master Category ID: " + relatedMasterCategoryID);
            errorMessage = string.Empty;
            List<MasterCategories> lstMasterSubCategories = new List<MasterCategories>();
            if (relatedMasterCategoryID > 0)
            {
                MasterCategories fetchedMasterCateg = Apps.dbContext.MasterCategories.Where(c => c.ID.Equals(relatedMasterCategoryID) && c.ISACTIVE.Equals(true)).FirstOrDefault();
                if (fetchedMasterCateg.REGCOMPANY.ID.Equals(HeaderInformation.CompanyID))
                {
                    if (fetchedMasterCateg.LSTSUBCATEGORIES != null)
                    {
                        fetchedMasterCateg.LSTSUBCATEGORIES.Where(d => d.ISACTIVE.Equals(true)).ToList().ForEach(c =>
                            {
                                MasterCategories masterCateg = new MasterCategories();
                                masterCateg = PopulateCategoryFields.AssignSubCategoryValues(masterCateg, c);
                                masterCateg.SELECTEDCATEG = PopulateCategoryFields.AssignSubCategoryValues(masterCateg.SELECTEDCATEG, fetchedMasterCateg);
                                lstMasterSubCategories.Add(masterCateg);
                            });
                    }

                    if (fetchedMasterCateg.LSTPRODUCTS != null)
                    {

                    }

                    if (fetchedMasterCateg.LSTFILTERS != null)
                    {

                    }
                    if (fetchedMasterCateg.LSTOFFERS != null)
                    {

                    }
                }
                else
                {
                    errorMessage = Messages.NotAuthorize;
                }
            }
            else
            {
                errorMessage = Messages.SubCateogryMsg + " " + Messages.DoesnotExists;
            }
            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);
            return lstMasterSubCategories;
        } 
        #endregion

        public string AddMasterCategory(MasterCategories masterCateg)
        {
            try
            {
                Apps.Logger.Info("Initiating AddMasterCategory.");
                string result = ValidateMasterCategory(masterCateg, "A");
                if (result.Equals(Messages.Succcess))
                {
                    MasterCategories mainMasterCateg = new MasterCategories();
                    mainMasterCateg = PopulateCategoryFields.AssignCreateMasterCategoriesValues(mainMasterCateg, masterCateg);
                    mainMasterCateg.ID = Apps.dbContext.MasterCategories.ToList().Count > 0 ? (Apps.dbContext.MasterCategories.Max(c => c.ID) + 1) : 1;
                    if (masterCateg.REGCOMPANY != null && masterCateg.REGCOMPANY.ID > 0)
                    {
                        mainMasterCateg.REGCOMPANY = Apps.dbContext.Company.ToList().Where(c => c.ID.Equals(masterCateg.REGCOMPANY.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.REGCOMPANY.NAME)).FirstOrDefault();
                    }

                    if (masterCateg.SELECTEDENUM != null && masterCateg.SELECTEDENUM.ID > 0)
                    {
                        mainMasterCateg.SELECTEDENUM = Apps.dbContext.MainEnum.ToList().Where(c => c.ID.Equals(masterCateg.SELECTEDENUM.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.SELECTEDENUM.NAME)).FirstOrDefault();
                    }

                    if (masterCateg.SELECTEDCATEG != null && masterCateg.SELECTEDCATEG.ID > 0)
                    {
                        mainMasterCateg.SELECTEDCATEG = Apps.dbContext.MasterCategories.ToList().Where(c => c.ID.Equals(masterCateg.SELECTEDCATEG.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.SELECTEDCATEG.NAME)).FirstOrDefault();
                    }

                    if(masterCateg.LSTFILTERS!=null)
                    {
                        masterCateg.LSTFILTERS.Where(c => c.ISACTIVE.Equals(true)).ToList().ForEach(c =>
                            {

                            });
                    }
                    mainMasterCateg.VERSION = 1;
                    Apps.dbContext.MasterCategories.Add(mainMasterCateg);
                    Apps.dbContext.SaveChanges();
                    Apps.Logger.Info("Master Category ID: " + mainMasterCateg.ID);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Add Master Category: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string UpdateMasterCategory(MasterCategories masterCateg)
        {
            try
            {
                Apps.Logger.Info("Initiating UpdateMasterCategory");
                string result = ValidateMasterCategory(masterCateg, "U");
                if (result.Equals(Messages.Succcess))
                {
                    MasterCategories mainMasterCateg = Apps.dbContext.MasterCategories.Where(c => c.ID.Equals(masterCateg.ID)).FirstOrDefault();
                    int currentVersion = mainMasterCateg.VERSION;
                    if (mainMasterCateg != null)
                    {
                        mainMasterCateg = PopulateCategoryFields.AssignCreateMasterCategoriesValues(mainMasterCateg, masterCateg);
                        if (masterCateg.REGCOMPANY != null && masterCateg.REGCOMPANY.ID > 0)
                        {
                            mainMasterCateg.REGCOMPANY = Apps.dbContext.Company.ToList().Where(c => c.ID.Equals(masterCateg.REGCOMPANY.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.REGCOMPANY.NAME)).FirstOrDefault();
                        }

                        if (masterCateg.SELECTEDENUM != null && masterCateg.SELECTEDENUM.ID > 0)
                        {
                            mainMasterCateg.SELECTEDENUM = Apps.dbContext.MainEnum.ToList().Where(c => c.ID.Equals(masterCateg.SELECTEDENUM.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.SELECTEDENUM.NAME)).FirstOrDefault();
                        }

                        if (masterCateg.SELECTEDCATEG != null && masterCateg.SELECTEDCATEG.ID > 0)
                        {
                            mainMasterCateg.SELECTEDCATEG = Apps.dbContext.MasterCategories.ToList().Where(c => c.ID.Equals(masterCateg.SELECTEDCATEG.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.SELECTEDCATEG.NAME)).FirstOrDefault();
                        }

                        if (masterCateg.LSTFILTERS != null)
                        {
                            masterCateg.LSTFILTERS.Where(c => c.ISACTIVE.Equals(true)).ToList().ForEach(c =>
                            {

                            });
                        }
                        mainMasterCateg.VERSION = currentVersion + 1;
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Master Category ID: " + mainMasterCateg.ID);
                    }
                    else
                    {
                        result = Messages.UnableToFind + " " + Messages.CateogryMsg;
                    }
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Update Master Category: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteMasterCategory(MasterCategories masterCateg)
        {
            try
            {
                Apps.Logger.Info("Initiating DeleteMasterCategory.");
                string result = ValidateMasterCategory(masterCateg, "D");
                if (result.Equals(Messages.Succcess))
                {
                    MasterCategories mainMasterCateg = Apps.dbContext.MasterCategories.Where(c => c.ID.Equals(masterCateg.ID) && c.ISACTIVE.Equals(true)).FirstOrDefault();
                    if (mainMasterCateg != null)
                    {
                        mainMasterCateg.ISACTIVE = false;
                        mainMasterCateg.VERSION += 1;
                        Apps.dbContext.SaveChanges();
                        Apps.Logger.Info("Master Category ID: " + mainMasterCateg.ID);
                    }
                    else
                    {
                        result = Messages.UnableToFind + " " + Messages.CateogryMsg;
                    }
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while DeleteMasterCategory: " + ex.Message, ex);
                return ex.Message;
            }
        }

        #region Validate Master Category
        private string ValidateMasterCategory(MasterCategories masterCateg, string code)
        {
            if (masterCateg != null)
            {
                switch (code)
                {
                    case "A":
                        #region Adding Validation
                        if (masterCateg != null && !string.IsNullOrEmpty(masterCateg.NAME) && !string.IsNullOrEmpty(masterCateg.IMAGELOCATION) && masterCateg.REGCOMPANY.ID > 0 && !string.IsNullOrEmpty(masterCateg.REGCOMPANY.NAME))
                        {
                            if (!masterCateg.REGCOMPANY.ID.Equals(HeaderInformation.CompanyID))
                                return Messages.CompanyIDLoginDoesNotMatch;

                            if (!Apps.dbContext.MasterCategories.ToList().Exists(c => c.NAME.Equals(masterCateg.NAME) && (c.SELECTEDCATEG != null && c.SELECTEDCATEG.ID.Equals(masterCateg.SELECTEDCATEG.ID)) && c.REGCOMPANY.ID.Equals(HeaderInformation.CompanyID)))
                            {
                                if ((masterCateg.SELECTEDCATEG != null && !string.IsNullOrEmpty(masterCateg.SELECTEDCATEG.NAME) && masterCateg.SELECTEDCATEG.ID > 0) || (masterCateg.SELECTEDENUM != null && masterCateg.SELECTEDENUM.ID > 0 && !string.IsNullOrEmpty(masterCateg.SELECTEDENUM.NAME)))
                                {
                                    if (Apps.dbContext.Company.ToList().Exists(c => c.ID.Equals(masterCateg.REGCOMPANY.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.REGCOMPANY.NAME)))
                                    {
                                        if ((masterCateg.SELECTEDCATEG != null && Apps.dbContext.MasterCategories.ToList().Exists(c => c.ID.Equals(masterCateg.SELECTEDCATEG.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.SELECTEDCATEG.NAME)))
                                            || (masterCateg.SELECTEDENUM != null && Apps.dbContext.MainEnum.ToList().Exists(c => c.ID.Equals(masterCateg.SELECTEDENUM.ID) && c.NAME.Equals(masterCateg.SELECTEDENUM.NAME))))
                                        {
                                            return Messages.Succcess;
                                        }
                                        else
                                        {
                                            return Messages.Attached + " " + Messages.TopCategory + " " + Messages.Or + " " + Messages.CateogryMsg + " " + Messages.DoesnotExists;
                                        }
                                    }
                                    else
                                    {
                                        return Messages.Related + " " + Messages.Company + " " + Messages.DoesnotExists;
                                    }
                                }
                                else
                                {
                                    return Messages.RelationFailed;
                                }
                            }
                            else
                            {
                                return Messages.CateogryMsg + " " + Messages.AlreadyExists;
                            }
                        }
                        else
                        {
                            return Messages.RequiredFieldsEmpty;
                        }
                        #endregion
                    case "U":
                    case "D":
                        #region Updating or Deletion Validation
                        if (masterCateg != null && masterCateg.ID > 0 && !string.IsNullOrEmpty(masterCateg.NAME) && !string.IsNullOrEmpty(masterCateg.IMAGELOCATION) && masterCateg.REGCOMPANY.ID > 0 && !string.IsNullOrEmpty(masterCateg.REGCOMPANY.NAME))
                        {
                            if ((masterCateg.SELECTEDCATEG != null && !string.IsNullOrEmpty(masterCateg.SELECTEDCATEG.NAME) && masterCateg.SELECTEDCATEG.ID > 0) || (masterCateg.SELECTEDENUM != null && masterCateg.SELECTEDENUM.ID > 0 && !string.IsNullOrEmpty(masterCateg.SELECTEDENUM.NAME)))
                            {
                                if (!masterCateg.REGCOMPANY.ID.Equals(HeaderInformation.CompanyID))
                                    return Messages.CompanyIDLoginDoesNotMatch;

                                if (Apps.dbContext.MasterCategories.ToList().Exists(c => c.ID.Equals(masterCateg.ID)))
                                {
                                    if (masterCateg.REGCOMPANY != null && Apps.dbContext.Company.ToList().Exists(c => c.ID.Equals(masterCateg.REGCOMPANY.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.REGCOMPANY.NAME)))
                                    {
                                        if ((masterCateg.SELECTEDCATEG != null && Apps.dbContext.MasterCategories.ToList().Exists(c => c.ID.Equals(masterCateg.SELECTEDCATEG.ID) && c.ISACTIVE.Equals(true) && c.NAME.Equals(masterCateg.SELECTEDCATEG.NAME)))
                                            || (masterCateg.SELECTEDENUM != null && Apps.dbContext.MainEnum.ToList().Exists(c => c.ID.Equals(masterCateg.SELECTEDENUM.ID) && c.NAME.Equals(masterCateg.SELECTEDENUM.NAME))))
                                        {
                                            return Messages.Succcess;
                                        }
                                        else
                                        {
                                            return Messages.Attached + " " + Messages.TopCategory + " " + Messages.Or + " " + Messages.CateogryMsg + " " + Messages.DoesnotExists;
                                        }
                                    }
                                    else
                                    {
                                        return Messages.Related + " " + Messages.Company + " " + Messages.DoesnotExists;
                                    }
                                }
                                else
                                {
                                    return Messages.CateogryMsg + " " + Messages.DoesnotExists;
                                }
                            }
                            else
                            {
                                return Messages.RelationFailed;
                            }
                        }
                        else
                        {
                            return Messages.RequiredFieldsEmpty;
                        }
                        #endregion
                    default:
                        return Messages.UnKnown;
                }
            }
            else
            {
                return Messages.InvalidObject;
            }
        } 
        #endregion
    }
}
