﻿using MyfashionsDB.DBEnums;
using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.BusinessLayer.Filters
{
    public class MasterFiltersOperationsLogic
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public MasterFiltersOperationsLogic(LoginInfoCache headerInfo)
        {
            this.HeaderInformation = headerInfo;
        }
        public List<MasterFilters> GetAllFiltersUnderCompany(long relatedCompanyID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetAllFiltersUnderCompany. Related Company ID: " + relatedCompanyID);
            List<MasterFilters> lstFilters = new List<MasterFilters>();
            errorMessage = string.Empty;
            if (relatedCompanyID > 0)
            {
                if (relatedCompanyID.Equals(HeaderInformation.CompanyID))
                {
                    MainEnum masterEnum = Apps.dbContext.MainEnum.Where(c => c.ID.Equals((long)TopEnums.Filters) && c.LSTFILTERS.Any(d => d.REGCOMPANY.ID.Equals(relatedCompanyID))).FirstOrDefault();
                    if (masterEnum != null)
                    {
                        masterEnum.LSTFILTERS.Where(c => c.REGCOMPANY.ID.Equals(relatedCompanyID)).ToList().ForEach(c =>
                            {
                                MasterFilters masterFilter = new MasterFilters();
                                masterFilter = PopulateFiltersFields.AssignFilterValues(masterFilter, c);
                                lstFilters.Add(masterFilter);
                            });
                    }
                }
                else
                {
                    errorMessage = Messages.NotAuthorize;
                }
            }
            else
            {
                errorMessage = Messages.Company + " " + Messages.DoesnotExists;
            }
            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);
            return lstFilters;
        }

        public List<MasterFilters> GetFiltersUnderStore(long relatedStoreID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetFiltersUnderStore. Related Store ID: " + relatedStoreID);
            errorMessage = string.Empty;
            List<MasterFilters> lstFilters = new List<MasterFilters>();
            if (relatedStoreID > 0)
            {
                Stores storeInfo = Apps.dbContext.Stores.Where(c => c.ID.Equals(relatedStoreID)).FirstOrDefault();
                if (storeInfo != null)
                {
                    if (storeInfo.REGISTEREDCOMPANY.ID.Equals(HeaderInformation.CompanyID))
                    {
                        MainEnum masterEnum = Apps.dbContext.MainEnum.Where(c => c.ID.Equals((long)TopEnums.Filters)).FirstOrDefault();
                        //MainEnum masterEnum = Apps.dbContext.MainEnum.Where(c => c.ID.Equals((long)TopEnums.Filters) && c.LSTFILTERS.Any(d => d.LSTREGSTORE.Equals(storeInfo))).FirstOrDefault();
                        if (masterEnum != null && masterEnum.LSTFILTERS != null)
                        {
                            masterEnum.LSTFILTERS.Where(c => c.LSTREGSTORE.Equals(storeInfo)).ToList().ForEach(c =>
                                {
                                    MasterFilters masterFilter = new MasterFilters();
                                    masterFilter = PopulateFiltersFields.AssignFiltersBasedOnStore(storeInfo, masterFilter, c);
                                    lstFilters.Add(masterFilter);
                                });
                        }
                        else
                        {
                            errorMessage = Messages.Filter + " " + Messages.DoesnotExists;
                        }
                    }
                    else
                    {
                        errorMessage = Messages.NotAuthorize;
                    }
                }
                else
                {
                    errorMessage = Messages.StoreMsg + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                errorMessage = Messages.StoreMsg + " " + Messages.DoesnotExists;
            }
            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);
            return lstFilters;
        }

        public List<MasterFilters> GetFilterUnderCategoryID(long categoryID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetFiltersUnderStore. Related Store ID: " + categoryID);
            errorMessage = string.Empty;
            List<MasterFilters> lstFilters = new List<MasterFilters>();
            MasterCategories selectedCategory = Apps.dbContext.MasterCategories.FirstOrDefault(c => c.ID.Equals(categoryID));
            if (categoryID > 0 && selectedCategory != null)
            {
                if (selectedCategory.LSTPRODUCTS != null && selectedCategory.LSTPRODUCTS.Count > 0)
                {
                    List<long> lstPrdsId = selectedCategory.LSTPRODUCTS.Select(c => c.ID).ToList();

                    List<MasterFilters> lstMasterFilters = Apps.dbContext.MasterFilters.Where(c => c.LSTPRODUCTS.Any(d => lstPrdsId.Contains(d.ID))).ToList();

                    //if (selectedCategory.LSTFILTERS != null && selectedCategory.LSTFILTERS.Count>0)
                    //{
                    //    List<MasterFilters> lstFilteredFilters = selectedCategory.LSTFILTERS.Where(c => c.SELECTEDENUM != null).ToList();

                    //    lstFilteredFilters.ForEach(c =>
                    //    {
                    //        MasterFilters masterFilter = new MasterFilters();
                    //        masterFilter = PopulateFiltersFields.AssignFilterAndItsAssociates(masterFilter, c, lstMasterFilters);
                    //        lstFilters.Add(masterFilter);
                    //    });
                    //}
                    //else
                    //{
                    //    lstMasterFilters.ForEach(c =>
                    //    {
                    //        MasterFilters masterFilter = new MasterFilters();
                    //        masterFilter = PopulateFiltersFields.AssignFilterAndItsAssociates(masterFilter, c, lstMasterFilters);
                    //        lstFilters.Add(masterFilter);
                    //    });
                    //}
                    List<MasterFilters> lstFilteredFilters = Apps.dbContext.MasterFilters.Where(c => c.SELECTEDENUM != null).ToList();

                    lstFilteredFilters.ForEach(c =>
                    {
                        MasterFilters masterFilter = new MasterFilters();
                        masterFilter = PopulateFiltersFields.AssignFilterAndItsAssociates(masterFilter, c, lstMasterFilters);
                        lstFilters.Add(masterFilter);
                    });

                    for (int i = 0; i < lstFilters.Count; i++)
                    {
                        if (lstFilters[i].LSTSUBFILTERS == null && lstFilters[i].LSTSUBFILTERS.Count == 0)
                        {
                            lstFilters.Remove(lstFilters[i]);
                        }
                    }
                }
                else
                {
                    errorMessage = Messages.ProductMsg + " " + Messages.DoesnotExists + " " + Messages.Under + " " + Messages.CateogryMsg;
                }
            }
            else
            {
                errorMessage = Messages.CateogryMsg + " " + Messages.DoesnotExists;
            }
            Apps.Logger.Info((!string.IsNullOrEmpty(errorMessage)) ? ("Error Message: " + errorMessage) : ("Result: " + Messages.Succcess));
            Apps.Logger.Info("Filters Count: " + lstFilters.Count);
            return lstFilters;
        }

        public List<MasterFilters> GetFiltersByOfferID(long offerID, long storeId, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetFiltersByOfferID. Offer ID: " + offerID);
            errorMessage = string.Empty;
            List<MasterFilters> lstFilters = new List<MasterFilters>();

            MasterOffers selectedOffer = Apps.dbContext.MasterOffers.FirstOrDefault(c => c.ID.Equals(offerID));
            if (offerID > 0 && selectedOffer != null)
            {
                if (selectedOffer.LSTPRODUCTS != null && selectedOffer.LSTPRODUCTS.Count > 0)
                {
                    List<long> lstPrdsId = selectedOffer.LSTPRODUCTS.Where(c => c.RELATEDSTORE.ID.Equals(storeId)).Select(c => c.ID).ToList();

                    List<MasterFilters> lstMasterFilters = Apps.dbContext.MasterFilters.Where(c => c.LSTPRODUCTS.Any(d => lstPrdsId.Contains(d.ID))).ToList();

                    List<MasterFilters> lstFilteredFilters = Apps.dbContext.MasterFilters.Where(c => c.SELECTEDENUM != null).ToList();

                    lstFilteredFilters.ForEach(c =>
                    {
                        MasterFilters masterFilter = new MasterFilters();
                        masterFilter = PopulateFiltersFields.AssignFilterAndItsAssociates(masterFilter, c, lstMasterFilters);
                        lstFilters.Add(masterFilter);
                    });

                    for (int i = 0; i < lstFilters.Count; i++)
                    {
                        if (lstFilters[i].LSTSUBFILTERS == null && lstFilters[i].LSTSUBFILTERS.Count == 0)
                        {
                            lstFilters.Remove(lstFilters[i]);
                        }
                    }
                }
                else
                {
                    errorMessage = Messages.ProductMsg + " " + Messages.DoesnotExists + " " + Messages.Under + " " + Messages.Offer;
                }
            }
            else
            {
                errorMessage = Messages.Offer + " " + Messages.DoesnotExists;
            }
            Apps.Logger.Info((!string.IsNullOrEmpty(errorMessage)) ? ("Error Message: " + errorMessage) : ("Result: " + Messages.Succcess));
            Apps.Logger.Info("Filters Count: " + lstFilters.Count);
            return lstFilters;
        }

        public List<MasterFilters> GetFiltersUnderNewArrivals(long arrivalDays, long storeId, long categoryId, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetFiltersUnderNewArrivals. Arrival Days: " + arrivalDays);
            errorMessage = string.Empty;
            List<MasterFilters> lstFilters = new List<MasterFilters>();
            if (arrivalDays > 0)
            {
                DateTime checkedDateTime = DateTime.Now.AddDays(-arrivalDays);
                List<Product> lstFetchedProducts = Apps.dbContext.Product.Where(c => c.LASTUPDATEDTIME >= checkedDateTime && c.RELATEDSTORE.ID.Equals(storeId) && c.RELATEDCATEGORIES.Any(d => d.ID.Equals(categoryId))).ToList();
                if (lstFetchedProducts != null && lstFetchedProducts.Count > 0)
                {
                    List<long> lstPrdsId = lstFetchedProducts.Select(c => c.ID).ToList();

                    List<MasterFilters> lstMasterFilters = Apps.dbContext.MasterFilters.Where(c => c.LSTPRODUCTS.Any(d => lstPrdsId.Contains(d.ID))).ToList();

                    List<MasterFilters> lstFilteredFilters = Apps.dbContext.MasterFilters.Where(c => c.SELECTEDENUM != null).ToList();

                    lstFilteredFilters.ForEach(c =>
                    {
                        MasterFilters masterFilter = new MasterFilters();
                        masterFilter = PopulateFiltersFields.AssignFilterAndItsAssociates(masterFilter, c, lstMasterFilters);
                        lstFilters.Add(masterFilter);
                    });

                    for (int i = 0; i < lstFilters.Count; i++)
                    {
                        if (lstFilters[i].LSTSUBFILTERS == null && lstFilters[i].LSTSUBFILTERS.Count == 0)
                        {
                            lstFilters.Remove(lstFilters[i]);
                        }
                    }
                }
                else
                {
                    errorMessage = Messages.ProductMsg + " " + Messages.NotFound;
                }
            }
            else
            {
                errorMessage = Messages.ArrivalDays + " " + Messages.ShouldNotBeZero;
            }
            Apps.Logger.Info((!string.IsNullOrEmpty(errorMessage)) ? ("Error Message: " + errorMessage) : ("Result: " + Messages.Succcess));
            Apps.Logger.Info("Filters Count: " + lstFilters.Count);
            return lstFilters;
        }

        public MasterFilters GetFilterByItsFilterID(long filterID, out string errorMessage)
        {
            Apps.Logger.Info("Initiating GetFilterByItsFilterID. Filter ID: " + filterID);
            errorMessage = string.Empty;
            MasterFilters masterFilter = new MasterFilters();
            if (filterID > 0)
            {
                MasterFilters fetchedFilter = Apps.dbContext.MasterFilters.Where(c => c.ID.Equals(filterID)).FirstOrDefault();
                if (fetchedFilter != null)
                {
                    masterFilter = PopulateFiltersFields.AssignFilterValues(masterFilter, fetchedFilter);
                }
                else
                {
                    errorMessage = Messages.Filter + " " + Messages.DoesnotExists;
                }
            }
            else
            {
                errorMessage = Messages.Filter + " " + Messages.DoesnotExists;
            }
            if (!string.IsNullOrEmpty(errorMessage))
                Apps.Logger.Info("Error Message: " + errorMessage);
            else
                Apps.Logger.Info("Result: " + Messages.Succcess);
            return masterFilter;
        }

        public string AddFilter(MasterFilters masterFilter)
        {
            try
            {
                Apps.Logger.Info("Initiating AddFilter.");
                string result = ValidateMasterFilter(masterFilter, "A");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnFilters.AddMasterFilter(masterFilter);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Add Filter: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string UpdateFilter(MasterFilters masterFilter)
        {
            try
            {
                Apps.Logger.Info("Initiating UpdateFilter.");
                string result = ValidateMasterFilter(masterFilter, "U");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnFilters.UpdateMasterFilter(masterFilter);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Update Filter: " + ex.Message, ex);
                return ex.Message;
            }
        }

        public string DeleteFilter(MasterFilters masterFilter)
        {
            try
            {
                Apps.Logger.Info("Initiating DeleteFilter.");
                string result = ValidateMasterFilter(masterFilter, "D");
                if (result.Equals(Messages.Succcess))
                {
                    result = CRUDOperationsOnFilters.DeleteMasterFilter(masterFilter);
                }
                Apps.Logger.Info("Result: " + result);
                return result;
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception while Delete Filter: " + ex.Message, ex);
                return ex.Message;
            }
        }

        private string ValidateMasterFilter(MasterFilters masterFilter, string code)
        {
            if (masterFilter != null)
          {
                switch (code)
                {
                    case "A":
                    #region Adding Validation
                        if (!string.IsNullOrEmpty(masterFilter.NAME) && masterFilter.REGCOMPANY != null && masterFilter.REGCOMPANY.ID > 0 && masterFilter.LSTREGSTORE != null && masterFilter.LSTREGSTORE.Count > 0)
                        {
                            if (!Apps.dbContext.Company.Any(c => c.ID.Equals(masterFilter.REGCOMPANY.ID)))
                                return Messages.Company + " " + Messages.DoesnotExists;

                            if (masterFilter.LSTREGSTORE != null)
                            {
                                foreach (Stores item in masterFilter.LSTREGSTORE)
                                {
                                    if (!Apps.dbContext.Stores.Any(c => c.ID.Equals(item.ID) && c.REGISTEREDCOMPANY.ID.Equals(masterFilter.REGCOMPANY.ID)))
                                    {
                                        return Messages.StoreMsg + " " + Messages.DoesnotExists;
                                    }
                                }
                            }

                            if (masterFilter.SELECTEDENUM != null)
                            {
                                if (!Apps.dbContext.MainEnum.Any(c => c.ID.Equals(masterFilter.SELECTEDENUM.ID) && c.ID.Equals((long)TopEnums.Filters)))
                                    return Messages.MainEnum + " " + Messages.DoesnotExists;
                            }
                        }
                        else
                        {
                            return Messages.RequiredFieldsEmpty;
                        }
                        return Messages.Succcess;
                    #endregion
                    case "U":
                    case "D":
                        if (!string.IsNullOrEmpty(masterFilter.NAME) && masterFilter.SNO > 0 && masterFilter.ID > 0 && masterFilter.REGCOMPANY != null && masterFilter.REGCOMPANY.ID > 0 && masterFilter.LSTREGSTORE != null && masterFilter.LSTREGSTORE.Count > 0)
                        {
                            if (Apps.dbContext.MasterFilters.Any(c => c.ID.Equals(masterFilter.ID)))
                            {
                                if (!Apps.dbContext.Company.Any(c => c.ID.Equals(masterFilter.REGCOMPANY.ID)))
                                    return Messages.Company + " " + Messages.DoesnotExists;

                                if (!Apps.dbContext.MasterFilters.Any(c => c.ID.Equals(masterFilter.ID) && c.REGCOMPANY.ID.Equals(masterFilter.REGCOMPANY.ID)))
                                    return Messages.Filter + " " + Messages.DoesnotExists;

                                if (masterFilter.LSTREGSTORE != null)
                                {
                                    foreach (Stores item in masterFilter.LSTREGSTORE)
                                    {
                                        if (!Apps.dbContext.Stores.Any(c => c.ID.Equals(item.ID) && c.REGISTEREDCOMPANY.ID.Equals(masterFilter.REGCOMPANY.ID)))
                                        {
                                            return Messages.StoreMsg + " " + Messages.DoesnotExists;
                                        }
                                    }
                                }

                                if (masterFilter.LSTAPPLIEDSUBCATEGORIES != null)
                                {
                                    foreach (MasterCategories item in masterFilter.LSTAPPLIEDSUBCATEGORIES)
                                    {
                                        if (!Apps.dbContext.MasterCategories.Any(c => c.ID.Equals(item.ID) && c.NAME.Equals(item.NAME)))
                                            return Messages.Attached + " " + Messages.CateogryMsg + " " + Messages.DoesnotExists;
                                    }
                                }

                                if (masterFilter.MARKETPRICE != null)
                                {
                                    if (!Apps.dbContext.MarketPrice.Any(c => c.ID.Equals(masterFilter.MARKETPRICE.ID) && c.PRICE.Equals(masterFilter.MARKETPRICE.PRICE)))
                                        return Messages.MarketPrice + " " + Messages.DoesnotExists;
                                }

                                if (masterFilter.SELECTEDENUM != null)
                                {
                                    if (!Apps.dbContext.MainEnum.Any(c => c.ID.Equals(masterFilter.SELECTEDENUM.ID) && c.ID.Equals((long)TopEnums.Filters)))
                                        return Messages.MainEnum + " " + Messages.DoesnotExists;
                                }
                            }
                            else
                            {
                                return Messages.Filter + " " + Messages.DoesnotExists;
                            }
                        }
                        else
                        {
                            return Messages.RequiredFieldsEmpty;
                        }
                        return Messages.Succcess;
                    default:
                        break;
                }
                return string.Empty;
            }
            else
                return Messages.ObjectShouldNotBeNull;
        }
    }
}
