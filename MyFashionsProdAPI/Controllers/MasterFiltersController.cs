﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Filters;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class MasterFiltersController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public MasterFiltersController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetAllFiltersUnderCompany")]
        [HttpGet]
        public HttpResponseMessage GetAllFiltersUnderCompany(long relatedcompanyid)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetAllFiltersUnderCompany. Related Company ID: " + relatedcompanyid);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic(HeaderInformation);
            List<MasterFilters> lstMasterFilters = masterFilterLogic.GetAllFiltersUnderCompany(relatedcompanyid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterFilters>>(HttpStatusCode.OK, lstMasterFilters);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetFiltersUnderStore")]
        [HttpGet]
        public HttpResponseMessage GetFiltersUnderStore(long relatedStoreID)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetFiltersUnderStore. Related Store ID: " + relatedStoreID);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic(HeaderInformation);
            List<MasterFilters> lstMasterFilters = masterFilterLogic.GetFiltersUnderStore(relatedStoreID, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterFilters>>(HttpStatusCode.OK, lstMasterFilters);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetFiltersUnderCategoryID")]
        [HttpGet]
        public HttpResponseMessage GetFiltersUnderCategoryID(long categoryID)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetFiltersUnderCategoryID. Category ID: " + categoryID);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic(HeaderInformation);
            List<MasterFilters> lstFetchedFilter = masterFilterLogic.GetFilterUnderCategoryID(categoryID, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterFilters>>(HttpStatusCode.OK, lstFetchedFilter);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetFiltersByOfferID")]
        [HttpGet]
        public HttpResponseMessage GetFiltersByOfferID(long offerID, long storeId)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetFiltersByOfferID. Offer ID: " + offerID);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic(HeaderInformation);
            List<MasterFilters> lstFetchedFilter = masterFilterLogic.GetFiltersByOfferID(offerID, storeId, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterFilters>>(HttpStatusCode.OK, lstFetchedFilter);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetFiltersUnderNewArrivals")]
        [HttpGet]
        public HttpResponseMessage GetFiltersUnderNewArrivals(long arrivalDays, long storeId, long categoryId)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetFiltersUnderNewArrivals. Arrival Days: " + arrivalDays);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic(HeaderInformation);
            List<MasterFilters> lstFetchedFilter = masterFilterLogic.GetFiltersUnderNewArrivals(arrivalDays, storeId, categoryId, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterFilters>>(HttpStatusCode.OK, lstFetchedFilter);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetFilterByItsFilterIDAndStoreID")]
        [HttpGet]
        public HttpResponseMessage GetFilterByItsFilterID(long filterID, long storeId)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetFilterByItsFilterID. Filter ID: " + filterID);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic(HeaderInformation);
            MasterFilters fetchedFilter = masterFilterLogic.GetFilterByItsFilterID(filterID, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<MasterFilters>(HttpStatusCode.OK, fetchedFilter);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }
        [ActionName("GetFilterByItsFilterID")]
        [HttpGet]
        public HttpResponseMessage GetFilterByItsFilterID(long filterID)
        {
            Apps.Logger.Info("Initiated MasterFiltersController -> GetFilterByItsFilterID. Filter ID: " + filterID);
            string errorMsg = string.Empty;
            MasterFiltersOperationsLogic masterFilterLogic = new MasterFiltersOperationsLogic(HeaderInformation);
            MasterFilters fetchedFilter = masterFilterLogic.GetFilterByItsFilterID(filterID, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<MasterFilters>(HttpStatusCode.OK, fetchedFilter);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("MasterFiltersCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage MasterFiltersCRUDOperation(MasterFilters masterFilter, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(masterFilter);
            Apps.Logger.Info("Initiated MasterFiltersController -> MasterFiltersCRUDOperation.");
            Apps.Logger.Info("Master Filter Object: " + serializedObject);
            Apps.Logger.Info("Paramter: " + parameter);
            string result = Messages.InvalidCRUDOperation;
            MasterFiltersOperationsLogic masterFilterOperations = new MasterFiltersOperationsLogic(HeaderInformation);
            switch (parameter)
            {
                case "C":
                    result = masterFilterOperations.AddFilter(masterFilter);
                    break;
                case "U":
                    result = masterFilterOperations.UpdateFilter(masterFilter);
                    break;
                case "D":
                    result = masterFilterOperations.DeleteFilter(masterFilter);
                    break;
                default:
                    break;
            }

            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}