﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Product_Logic;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class MasterProductsController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public MasterProductsController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetAllProductsUnderStore")]
        [HttpGet]
        public HttpResponseMessage GetAllProductsUnderStore(long storeID, int pageNumber)
        {
            Apps.Logger.Info("Initiated MasterProductsController -> GetAllProductsUnderStore. StoreID: " + storeID);
            string result = string.Empty;
            if (pageNumber < 0)
            {
                pageNumber = 1;
            }

            ProductsOperationLogic prodOperObj = new ProductsOperationLogic(HeaderInformation);
            List<Product> lstProducts = prodOperObj.GetAllProductsUnderStore(storeID, pageNumber, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<Product>>(HttpStatusCode.OK, lstProducts);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("GetProductByProductID")]
        [HttpGet]
        public HttpResponseMessage GetProductByProductID(long productID)
        {
            Apps.Logger.Info("Initiated MasterProductsController -> GetProductByProductID. ProductID: " + productID);
            string result = string.Empty;
            ProductsOperationLogic prodOperObj = new ProductsOperationLogic(HeaderInformation);
            Product productInfo = prodOperObj.GetProductInformation(productID, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<Product>(HttpStatusCode.OK, productInfo);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }


        [ActionName("GetProductsByCategoryID")]
        [HttpGet]
        public HttpResponseMessage GetProductsByCategoryID(long categoryId, long storeId, int pageNumber, bool tempParam2)
        {
            Apps.Logger.Info("Initiated MasterProductsController -> GetProductsByCategoryID.");
            string result = string.Empty;
            ProductsOperationLogic prodOperObj = new ProductsOperationLogic(HeaderInformation);
            List<Product> lstProducts = prodOperObj.GetProductsByCategoryID(categoryId, storeId, pageNumber, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<Product>>(HttpStatusCode.OK, lstProducts);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("GetProductsUnderNewArrivals")]
        [HttpGet]
        public HttpResponseMessage GetProductsUnderNewArrivals(long arrivalDays, long storeId, int pageNumber, long categoryID, bool tempParam2)
        {
            Apps.Logger.Info("Initiated MasterProductsController -> GetProductsUnderNewArrivals.");
            string result = string.Empty;
            ProductsOperationLogic prodOperObj = new ProductsOperationLogic(HeaderInformation);
            List<Product> lstProducts = prodOperObj.GetProductsUnderNewArrivals(arrivalDays, storeId, pageNumber, categoryID, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<Product>>(HttpStatusCode.OK, lstProducts);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("GetAllBasicProductInforation")]
        [HttpGet]
        public HttpResponseMessage GetAllBasicProductInforation(long categoryId, long storeId, bool tempParam2)
        {
            Apps.Logger.Info("Initiated MasterProductsController -> GetProductsByCategoryID.");
            string result = string.Empty;
            ProductsOperationLogic prodOperObj = new ProductsOperationLogic(HeaderInformation);
            List<Product> lstProducts = prodOperObj.GetAllBasicProductInforation(categoryId, storeId, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<Product>>(HttpStatusCode.OK, lstProducts);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("GetAllProductImages")]
        [HttpGet]
        public HttpResponseMessage GetAllProductImages()
        {
            Apps.Logger.Info("Initiated MasterProductsController -> GetAllProductImages.");
            string result = string.Empty;
            ProductsOperationLogic prodOperObj = new ProductsOperationLogic(HeaderInformation);
            List<ProductImageCollection> lstProductImages = prodOperObj.GetAllProductImages(out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<ProductImageCollection>>(HttpStatusCode.OK, lstProductImages);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("MasterProductCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage MasterProductCRUDOperation(Product masterProd, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(masterProd);
            Apps.Logger.Info("Initiated MasterProductsController -> MasterProductCRUDOperation.");
            Apps.Logger.Info("Product Object: " + serializedObject);
            Apps.Logger.Info("Paramter: " + parameter);
            string result = Messages.InvalidCRUDOperation;
            ProductsOperationLogic productOperObj = new ProductsOperationLogic(HeaderInformation);
            switch (parameter)
            {
                case "C":
                    result = productOperObj.AddProduct(masterProd);
                    break;
                case "U":
                    result = productOperObj.UpdateProduct(masterProd);
                    break;
                case "D":
                    result = productOperObj.DeleteProduct(masterProd);
                    break;
                default:
                    break;
            }

            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}
