﻿using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.PriceRangeSlider;
using MyFashionsProd.API.Models.PriceRange;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class PriceRangeController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public PriceRangeController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetPriceRangeUnderStore")]
        [HttpGet]
        public HttpResponseMessage GetPriceRangeUnderStore(long storeId)
        {
            Apps.Logger.Info("Initiated PriceRangeController -> GetPriceRangeUnderStore. Store ID: " + storeId);
            string errorMsg = string.Empty;
            PriceRangeSliderOperationsLogic priceSliderlogic = new PriceRangeSliderOperationsLogic();
            PriceSlider priceSlider = priceSliderlogic.GetPriceUnderStoreId(storeId, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<PriceSlider>(HttpStatusCode.OK, priceSlider);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetPriceRangeUnderCategoryID")]
        [HttpGet]
        public HttpResponseMessage GetPriceRangeUnderCategoryID(long categoryId)
        {
            Apps.Logger.Info("Initiated PriceRangeController -> GetPriceRangeUnderCategoryID. Category ID: " + categoryId);
            string errorMsg = string.Empty;
            PriceRangeSliderOperationsLogic priceSliderlogic = new PriceRangeSliderOperationsLogic();
            PriceSlider priceSlider = priceSliderlogic.GetPriceUnderCategoryId(categoryId, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<PriceSlider>(HttpStatusCode.OK, priceSlider);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetPriceRangeUnderOffer")]
        [HttpGet]
        public HttpResponseMessage GetPriceRangeUnderOffer(long offerId)
        {
            Apps.Logger.Info("Initiated PriceRangeController -> GetPriceRangeUnderOffer. Offer ID: " + offerId);
            string errorMsg = string.Empty;
            PriceRangeSliderOperationsLogic priceSliderlogic = new PriceRangeSliderOperationsLogic();
            PriceSlider priceSlider = priceSliderlogic.GetPriceUnderOffer(offerId, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<PriceSlider>(HttpStatusCode.OK, priceSlider);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetPriceRangeUnderNewArrivals")]
        [HttpGet]
        public HttpResponseMessage GetPriceRangeUnderNewArrivals(long arrivalDays,long categoryId,long storeId)
        {
            Apps.Logger.Info("Initiated PriceRangeController -> GetPriceRangeUnderNewArrivals. Arrival Days: " + arrivalDays +"CategoryId"+categoryId+"StoreId"+storeId+"");
            string errorMsg = string.Empty;
            PriceRangeSliderOperationsLogic priceSliderlogic = new PriceRangeSliderOperationsLogic();
            PriceSlider priceSlider = priceSliderlogic.GetPriceUnderArrivalDays(arrivalDays,categoryId,storeId,out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<PriceSlider>(HttpStatusCode.OK, priceSlider);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }
    }
}
