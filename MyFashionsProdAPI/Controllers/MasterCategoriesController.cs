﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Categories;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class MasterCategoriesController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public MasterCategoriesController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetAllCategories")]
        [HttpGet]
        public HttpResponseMessage GetAllCategories(long relatedcompanyid)
        {
            Apps.Logger.Info("Initiated MasterCategoriesController -> GetAllCategories. Related Company ID: " + relatedcompanyid);
            string errorMsg = string.Empty;
            MasterCategoriesOperationsLogic masterCategOperObj = new MasterCategoriesOperationsLogic(HeaderInformation);
            MainEnum mainMasterCategories = masterCategOperObj.GetAllMainCategories(relatedcompanyid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
            {
                return Request.CreateResponse<MainEnum>(HttpStatusCode.OK, mainMasterCategories);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
            }
        }

        [ActionName("GetSubCategories")]
        [HttpGet]
        public HttpResponseMessage GetSubCategories(long relatedmastercategoryid)
        {
            Apps.Logger.Info("Initiated MasterCategoriesController -> GetSubCategories. Related Master Category ID: " + relatedmastercategoryid);
            string errorMsg = string.Empty;
            MasterCategoriesOperationsLogic masterCategOperObj = new MasterCategoriesOperationsLogic(HeaderInformation);
            List<MasterCategories> lstSubCategories = masterCategOperObj.GetSubCategories(relatedmastercategoryid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterCategories>>(HttpStatusCode.OK, lstSubCategories);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetSingleCategory")]
        [HttpGet]
        public HttpResponseMessage GetSingleCategory(long relatedcategoryid)
        {
            Apps.Logger.Info("Initiated MasterCategoriesController -> GetSingleCategory. Related Category ID: " + relatedcategoryid);
            string errorMsg = string.Empty;
            MasterCategoriesOperationsLogic masterCategOperObj = new MasterCategoriesOperationsLogic(HeaderInformation);
            MasterCategories relatedCategory = masterCategOperObj.GetSingleCategory(relatedcategoryid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<MasterCategories>(HttpStatusCode.OK, relatedCategory);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("MasterCategoriesCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage MasterCategoriesCRUDOperation(MasterCategories masterCateg, string parameter)
        {
            if (((int)HeaderInformation.Login) <= 1)
            {
                string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(masterCateg);
                Apps.Logger.Info("Initiated MasterCategoriesController -> MasterCategoriesCRUDOperation.");
                Apps.Logger.Info("MasterCategory Object: " + serializedObject);
                Apps.Logger.Info("Parameter: " + parameter);
                string result = Messages.InvalidCRUDOperation;
                MasterCategoriesOperationsLogic masterOperations = new MasterCategoriesOperationsLogic(HeaderInformation);
                switch (parameter)
                {
                    case "C":
                        result = masterOperations.AddMasterCategory(masterCateg);
                        break;
                    case "U":
                        result = masterOperations.UpdateMasterCategory(masterCateg);
                        break;
                    case "D":
                        result = masterOperations.DeleteMasterCategory(masterCateg);
                        break;
                    default:
                        break;
                }

                if (result.Equals(Messages.Succcess))
                {
                    return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }
    }
}
