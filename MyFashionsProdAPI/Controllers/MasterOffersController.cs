﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Offers;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class MasterOffersController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public MasterOffersController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }


        [ActionName("GetAllOffersUnderCompany")]
        [HttpGet]
        public HttpResponseMessage GetAllOffersUnderCompany(long relatedcompanyid)
        {
            Apps.Logger.Info("Initiated MasterOffersController -> GetAllOffersUnderCompany. Related Company ID: " + relatedcompanyid);
            string errorMsg = string.Empty;
            MasterOffersOperationsLogic masterOffers = new MasterOffersOperationsLogic(HeaderInformation);
            List<MasterOffers> lstMasterOffers = masterOffers.GetAllOffersUnderCompany(relatedcompanyid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterOffers>>(HttpStatusCode.OK, lstMasterOffers);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetAllOffersUnderStore")]
        [HttpGet]
        public HttpResponseMessage GetAllOffersUnderStore(long relatedstoreid)
        {
            Apps.Logger.Info("Initiated MasterOffersController -> GetAllOffersUnderStore. Related Store ID: " + relatedstoreid);
            string errorMsg = string.Empty;
            MasterOffersOperationsLogic masterOffers = new MasterOffersOperationsLogic(HeaderInformation);
            List<MasterOffers> lstMasterOffers = masterOffers.GetAllOffersUnderStore(relatedstoreid, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<List<MasterOffers>>(HttpStatusCode.OK, lstMasterOffers);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("GetOfferByMasterOfferID")]
        [HttpGet]
        public HttpResponseMessage GetOfferByMasterOfferID(long relatedMasterOfferID)
        {
            Apps.Logger.Info("Initiated MasterOffersController -> GetOfferByMasterOfferID. Related Master Offer ID: " + relatedMasterOfferID);
            string errorMsg = string.Empty;
            MasterOffersOperationsLogic masterOffers = new MasterOffersOperationsLogic(HeaderInformation);
            MasterOffers fetchedOffer = masterOffers.GetOfferByMasterOfferID(relatedMasterOfferID, out errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
                return Request.CreateResponse<MasterOffers>(HttpStatusCode.OK, fetchedOffer);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMsg));
        }

        [ActionName("MasterOffersCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage MasterOffersCRUDOperation(MasterOffers masterOffer, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(masterOffer);
            Apps.Logger.Info("Initiated MasterOffersController -> MasterOffersCRUDOperation.");
            Apps.Logger.Info("Master Offer Object: " + serializedObject);
            Apps.Logger.Info("Paramter: " + parameter);
            string result = Messages.InvalidCRUDOperation;
            MasterOffersOperationsLogic masterOfferOperations = new MasterOffersOperationsLogic(HeaderInformation);
            switch (parameter)
            {
                case "C":
                    result = masterOfferOperations.AddOffer(masterOffer);
                    break;
                case "U":
                    result = masterOfferOperations.UpdateOffer(masterOffer);
                    break;
                case "D":
                    result = masterOfferOperations.DeleteOffer(masterOffer);
                    break;
                default:
                    break;
            }

            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}
