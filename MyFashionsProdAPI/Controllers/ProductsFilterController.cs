﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.ProdutsFilterLogic;
using MyFashionsProd.API.Models.ProductsFilter;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class ProductsFilterController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public ProductsFilterController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetProducts")]
        [HttpPost]
        public HttpResponseMessage GetProducts(IntegratedFilter integratedFilter, int pageNumber)
        {
            Apps.Logger.Info("Initiated ProductsFilterController -> GetProducts. Page Number: " + pageNumber);
            string result = string.Empty;
            if (pageNumber < 0)
            {
                pageNumber = 1;
            }

            ProdutsFilterOperationsLogic prodOperObj = new ProdutsFilterOperationsLogic();
            List<Product> lstProducts = prodOperObj.GetProductsByFilter(integratedFilter, pageNumber, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<Product>>(HttpStatusCode.OK, lstProducts);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}