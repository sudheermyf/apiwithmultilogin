﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.ExpressInterests;
using MyFashionsProd.API.BusinessLayer.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MyFashionsProdAPI.Filters;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class ExpressInterestController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public ExpressInterestController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetExpressInterestUnderStore")]
        [HttpGet]
        public HttpResponseMessage GetExpressInterestUnderStore(long storeID)
        {
            Apps.Logger.Info("Initiated ExpressInterestController -> GetExpressInterestUnderStore. Store ID: " + storeID);
            string result = string.Empty;
            ExpressInterestOperationsLogic expressInterestServiceCalls = new ExpressInterestOperationsLogic(HeaderInformation);
            List<ExpressInterest> lstExpressInterests = expressInterestServiceCalls.GetExpressInterestUnderStore(storeID, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<ExpressInterest>>(HttpStatusCode.OK, lstExpressInterests);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("GetExpressInterestUnderCompany")]
        [HttpGet]
        public HttpResponseMessage GetExpressInterestUnderCompany(long companyID)
        {
            Apps.Logger.Info("Initiated ExpressInterestController -> GetExpressInterestUnderCompany. Company ID: " + companyID);
            string result = string.Empty;
            ExpressInterestOperationsLogic expressInterestServiceCalls = new ExpressInterestOperationsLogic(HeaderInformation);
            List<ExpressInterest> lstExpressInterests = expressInterestServiceCalls.GetExpressInterestUnderCompany(companyID, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<ExpressInterest>>(HttpStatusCode.OK, lstExpressInterests);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("GetExpressInterestUnderProduct")]
        [HttpGet]
        public HttpResponseMessage GetExpressInterestUnderProduct(long productID)
        {
            Apps.Logger.Info("Initiated ExpressInterestController -> GetExpressInterestUnderProduct. Product ID: " + productID);
            string result = string.Empty;
            ExpressInterestOperationsLogic expressInterestServiceCalls = new ExpressInterestOperationsLogic(HeaderInformation);
            List<ExpressInterest> lstExpressInterests = expressInterestServiceCalls.GetExpressInterestUnderProduct(productID, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<ExpressInterest>>(HttpStatusCode.OK, lstExpressInterests);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("GetExpressInterestUnderEmail")]
        [HttpGet]
        public HttpResponseMessage GetExpressInterestUnderEmail(string emailAddress)
        {
            Apps.Logger.Info("Initiated ExpressInterestController -> GetExpressInterestUnderEmail. EMail Address: " + emailAddress);
            string result = string.Empty;
            ExpressInterestOperationsLogic expressInterestServiceCalls = new ExpressInterestOperationsLogic(HeaderInformation);
            List<Product> lstExpressProducts = expressInterestServiceCalls.GetExpressInterestUnderEmail(emailAddress, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<Product>>(HttpStatusCode.OK, lstExpressProducts);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
        [ActionName("GetInterestUnderEmail")]
        [HttpGet]
        public HttpResponseMessage GetInterestUnderEmail(string emailAddress1,bool tempParam1)
        {
            Apps.Logger.Info("Initiated ExpressInterestController -> GetInterestUnderEmail. EMail Address: " + emailAddress1);
            string result = string.Empty;
            ExpressInterestOperationsLogic expressInterestServiceCalls = new ExpressInterestOperationsLogic(HeaderInformation);
            ExpressInterest expInterest = expressInterestServiceCalls.GetInterestUnderEmail(emailAddress1,tempParam1,out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<ExpressInterest>(HttpStatusCode.OK, expInterest);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("ExpressEnterestUpdateOperation")]
        [HttpGet]
        public HttpResponseMessage ExpressEnterestUpdateOperation(string emailAddress, string contactNumber, string Address)
        {
            Apps.Logger.Info("Initiated ExpressInterestController -> ExpressEnterestUpdateOperation.");
            Apps.Logger.Info("Express Interest EmailAddress: " + emailAddress);
            Apps.Logger.Info("Contact Number: " + contactNumber);
            Apps.Logger.Info("Address: " + Address);
            string result = string.Empty;
            ExpressInterestOperationsLogic expressInterestServiceCalls = new ExpressInterestOperationsLogic(HeaderInformation);
            result = expressInterestServiceCalls.UpdateExpressInterest(emailAddress, contactNumber, Address, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("DeleteExpressInterest")]
        [HttpGet]
        public HttpResponseMessage DeleteExpressInterest(long productId, string emailAddress)
        {
            string result = string.Empty;
            Apps.Logger.Info("Initiated ExpressInterestController -> ExpressInterestCRUDOperation.");
            Apps.Logger.Info("Product Id: " + productId);
            Apps.Logger.Info("Email Address: " + emailAddress);
            ExpressInterestOperationsLogic expressInterestServiceCalls = new ExpressInterestOperationsLogic(HeaderInformation);
            result = expressInterestServiceCalls.DeleteExpressInterest(productId, emailAddress);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("ExpressInterestCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage ExpressInterestCRUDOperation(ExpressInterest expressInterest, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(expressInterest);
            Apps.Logger.Info("Initiated ExpressInterestController -> ExpressInterestCRUDOperation.");
            Apps.Logger.Info("Express Interest Object: " + serializedObject);
            Apps.Logger.Info("Parameter: " + parameter);
            string result = Messages.InvalidCRUDOperation;
            ExpressInterestOperationsLogic expressInterestServiceCalls = new ExpressInterestOperationsLogic(HeaderInformation);
            switch (parameter)
            {
                case "C":
                    result = expressInterestServiceCalls.AddExpressInterest(expressInterest);
                    break;
                case "U":
                    result = Messages.InvalidCRUDOperation;
                    break;
                case "D":
                default:
                    result = Messages.InvalidCRUDOperation;
                    break;
            }
            if (result.Equals(Messages.Succcess) || result.Equals("Already your interest has been expressed to this product."))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}
