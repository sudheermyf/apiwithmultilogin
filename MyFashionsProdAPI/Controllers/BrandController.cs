﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Brands;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class BrandController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public BrandController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetAllBrandsUnderCompanyOrStore")]
        [HttpGet]
        public HttpResponseMessage GetAllBrandsUnderCompanyOrStore(long id, bool checkstore)
        {
            string result = string.Empty;
            if (checkstore)
            {
                if (!HeaderInformation.LstStoreIds.Contains(id))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
                }
            }
            else
            {
                if (!id.Equals(HeaderInformation.CompanyID))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
                }
            }

            BrandOperationsLogic brandOperations = new BrandOperationsLogic(HeaderInformation);
            brandOperations.ValidateGetAllBrands(id, checkstore, out result);

            if (result.Equals(Messages.Succcess))
            {
                List<Brand> lstBrands = brandOperations.GetAllBrands(id, checkstore, out result);
                if (result.Equals(Messages.Succcess))
                {
                    return Request.CreateResponse<List<Brand>>(HttpStatusCode.OK, lstBrands);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
        [ActionName("BrandCRUDOperation")]
        [HttpPost]
        public HttpResponseMessage BrandCRUDOperation(Brand brandObj, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(brandObj);
            Apps.Logger.Info("Initiated BrandController -> BrandCRUDOperation.");
            Apps.Logger.Info("Brand Object: " + serializedObject);
            Apps.Logger.Info("Parameter: " + parameter);
            BrandOperationsLogic brandOperObj = new BrandOperationsLogic(HeaderInformation);
            string result = Messages.InvalidCRUDOperation;
            switch (parameter)
            {
                case "C":
                    result = brandOperObj.AddBrand(brandObj);
                    break;
                case "U":
                    result = brandOperObj.UpdateBrand(brandObj);
                    break;
                case "D":
                    result = brandOperObj.DeleteBrand(brandObj);
                    break;
                default:
                    break;
            }

            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, result);
            }
        }
    }
}