﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Similar_Products_Logic;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class SimilarProductController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public SimilarProductController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetAllSimilarProductUnderProduct")]
        [HttpGet]
        public HttpResponseMessage GetAllSimilarProductUnderProduct(long productID)
        {
            Apps.Logger.Info("Initiated SimilarProductController -> GetAllSimilarProductUnderProduct. Product ID: " + productID);
            string result = string.Empty;
            SimilarProductOperationsLogic similarProdOperObj = new SimilarProductOperationsLogic(HeaderInformation);
            List<SimilarProduct> lstSimilarProds = similarProdOperObj.GetAllSimilarProductUnderProduct(productID, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<SimilarProduct>>(HttpStatusCode.OK, lstSimilarProds);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("AddOrupdateSimilarProduct")]
        [HttpPost]
        public HttpResponseMessage AddOrupdateSimilarProduct(Product prod)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(prod);
            Apps.Logger.Info("Initiated SimilarProductController -> AddOrupdateSimilarProduct.");
            Apps.Logger.Info("Product Object: " + serializedObject);
            string result = string.Empty;
            SimilarProductOperationsLogic similarProdOperObj = new SimilarProductOperationsLogic(HeaderInformation);
            result = similarProdOperObj.AddOrupdateSimilarProduct(prod);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("DeleteSimilarProduct")]
        [HttpPost]
        public HttpResponseMessage DeleteSimilarProduct(long similarProductID, bool isDelete)
        {
            Apps.Logger.Info("Initiated SimilarProductController -> DeleteSimilarProduct.");
            Apps.Logger.Info("Similar Product ID: " + similarProductID);
            string result = string.Empty;
            SimilarProductOperationsLogic similarProdOperObj = new SimilarProductOperationsLogic(HeaderInformation);
            result = similarProdOperObj.DeleteSimilarProduct(similarProductID);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}
