﻿using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.CartLogic;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class CartItemsController : ApiController
    {

        [ActionName("GetCartItemsUnderEmail")]
        [HttpGet]
        public HttpResponseMessage GetCartItemsUnderEmail(string emailAddress)
        {
            Apps.Logger.Info("Initiated CartItemsController -> GetCartItemsUnderEmail. EMail Address: " +
                             emailAddress);
            string result = string.Empty;
            CartOperationsLogic cartOperationsServiceCalls = new CartOperationsLogic();
            List<Product> lstCartItems = cartOperationsServiceCalls.GetCartItemsUnderEmail(emailAddress,
                out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<Product>>(HttpStatusCode.OK, lstCartItems);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("AddCartItem")]
        [HttpPost]
        public HttpResponseMessage AddCartItem(CartItems cartItem, string parameter)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(cartItem);
            Apps.Logger.Info("Initiated CartItemsController -> AddCartItem.");
            Apps.Logger.Info("Cart Item Object: " + serializedObject);
            Apps.Logger.Info("Parameter: " + parameter);
            string result = Messages.InvalidCRUDOperation;
            CartOperationsLogic cartOperationsServiceCalls = new CartOperationsLogic();
            switch (parameter)
            {
                case "C":
                    result = cartOperationsServiceCalls.AddCart(cartItem);
                    break;
                case "U":
                    result = Messages.InvalidCRUDOperation;
                    break;
                case "D":
                default:
                    result = Messages.InvalidCRUDOperation;
                    break;
            }
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("DeleteCartItem")]
        [HttpGet]
        public HttpResponseMessage DeleteCartItem(long productId, string emailAddress)
        {
            string result = string.Empty;
            Apps.Logger.Info("Initiated CartItemsController -> DeleteCartItem.");
            Apps.Logger.Info("Product Id: " + productId);
            Apps.Logger.Info("Email Address: " + emailAddress);
            CartOperationsLogic cartOperationsServiceCalls = new CartOperationsLogic();
            result = cartOperationsServiceCalls.DeleteCart(productId, emailAddress);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
        [ActionName("DeleteAllCartItemsUnderEmail")]
        [HttpGet]
        public HttpResponseMessage DeleteAllCartItemsUnderEmail(string masterEmailAddress)
        {
            string result = string.Empty;
            Apps.Logger.Info("Initiated CartItemsController -> DeleteAllCartItemsUnderEmail.");
            Apps.Logger.Info("Email Address: " + masterEmailAddress);
            CartOperationsLogic cartOperationsServiceCalls = new CartOperationsLogic();
            result = cartOperationsServiceCalls.DeleteCartItems(masterEmailAddress);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }


    }
}
