﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Authentication;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class SuperAdminController : ApiController
    {

        public LoginInfoCache HeaderInformation { get; set; }
        public SuperAdminController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }
        [ActionName("ChangeSuperAdminPasword")]
        [HttpGet]
        public HttpResponseMessage ChangeSuperAdminPasword(string username, string oldpassword, string newpassword)
        {
            if (((int)HeaderInformation.Login).Equals(0))
            {
                Apps.Logger.Info("Initiated SuperAdminController -> ChangeSuperAdminPasword. UserName: " + username + " OldPassword: " + oldpassword + " NewPassword: " + newpassword);
                username = username.ToUpper();
                oldpassword = oldpassword.ToUpper();
                newpassword = newpassword.ToUpper();
                long superAdminID = 0;
                SuperAdminLogic superObj = new SuperAdminLogic();
                string result = superObj.ValidateSuperAdmin(username, oldpassword, out superAdminID);
                if (result.Equals(Messages.Succcess))
                {
                    if (superObj.ChangePassword(username, oldpassword, newpassword))
                        return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
                    else
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.FailedUpdatingPassword);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }

        [ActionName("ValidateSuperAdmin")]
        [HttpGet]
        public HttpResponseMessage ValidateSuperAdmin(string username, string password)
        {
            if (((int)HeaderInformation.Login).Equals(0))
            {
                Apps.Logger.Info("Initiated SuperAdminController -> ValidateSuperAdmin. UserName: " + username + " Password: " + password);
                username = username.ToUpper();
                password = password.ToUpper();
                long superAdminID = 0;
                SuperAdminLogic superObj = new SuperAdminLogic();
                string result = superObj.ValidateSuperAdmin(username, password, out superAdminID);
                if (result.Equals(Messages.Succcess))
                {
                    return Request.CreateResponse<long>(HttpStatusCode.OK, superAdminID);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }

        [ActionName("GetAllSuperAdmins")]
        [HttpGet]
        public HttpResponseMessage GetAllSuperAdmins()
        {
            if (((int)HeaderInformation.Login).Equals(0))
            {
                Apps.Logger.Info("Initiated SuperAdminController -> GetAllSuperAdmins");
                SuperAdminLogic superObj = new SuperAdminLogic();
                List<SuperAdmin> lstSuperAdmins = superObj.GetAllSuperAdmins();
                return Request.CreateResponse<List<SuperAdmin>>(HttpStatusCode.OK, lstSuperAdmins);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }

        [ActionName("GetSuperAdminByID")]
        [HttpGet]
        public HttpResponseMessage GetSuperAdminByID(long superAdminID)
        {
            if (((int)HeaderInformation.Login).Equals(0))
            {
                Apps.Logger.Info("Initiated SuperAdminController -> GetSuperAdminByID. SuperAdmin ID: " + superAdminID);
                string errorMessage = string.Empty;
                SuperAdminLogic superObj = new SuperAdminLogic();
                SuperAdmin result = superObj.ValidateSuperAdmin(superAdminID, out errorMessage);
                if (string.IsNullOrEmpty(errorMessage))
                {
                    return Request.CreateResponse<SuperAdmin>(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(errorMessage));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError(Messages.PermissionDenied));
            }
        }

        [ActionName("InitiateSeed")]
        [HttpGet]
        public HttpResponseMessage InitiateSeed(bool initiateseed)
        {
            Apps.Logger.Info("Initiated SuperAdminController -> Initiated Seed. Initiate Seed: " + initiateseed);
            SuperAdminLogic superObj = new SuperAdminLogic();
            bool result = superObj.InitiateSeed();
            Apps.Logger.Info("Result: " + result);
            if (result)
                return Request.CreateResponse<bool>(HttpStatusCode.OK, result);
            else
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result.ToString()));
        }
    }
}
