﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Complete_Look_Logic;
using MyFashionsProdAPI.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MyFashionsProdAPI.Controllers
{

    [EnableCors("*", "*", "*")]
    [AuthenticationFilter]
    public class CompleteLookController : ApiController
    {
        public LoginInfoCache HeaderInformation { get; set; }
        public CompleteLookController()
        {
            Object header = null;
            if (Request.Properties.TryGetValue("HeaderInfo", out header))
            {
                HeaderInformation = (LoginInfoCache)header;
            }
        }

        [ActionName("GetAllCompleteLookUnderProduct")]
        [HttpGet]
        public HttpResponseMessage GetAllCompleteLookUnderProduct(long productID)
        {
            Apps.Logger.Info("Initiated CompleteLookController -> GetAllCompleteLookUnderProduct. Product ID: " + productID);
            string result = string.Empty;
            CompleteLookOperationsLogic completeLookOperObj = new CompleteLookOperationsLogic();
            List<CompleteLook> lstCompleteLooks = completeLookOperObj.GetAllCompleteLookUnderProduct(productID, out result);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<List<CompleteLook>>(HttpStatusCode.OK, lstCompleteLooks);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }

        [ActionName("AddOrupdateCompleteLook")]
        [HttpPost]
        public HttpResponseMessage AddOrupdateCompleteLook(Product prod)
        {
            string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(prod);
            Apps.Logger.Info("Initiated CompleteLookController -> AddOrupdateCompleteLook.");
            Apps.Logger.Info("Product Object: " + serializedObject);
            string result = string.Empty;
            CompleteLookOperationsLogic completeLookOperObj = new CompleteLookOperationsLogic();
            result = completeLookOperObj.AddOrupdateCompleteLook(prod);
            if (result.Equals(Messages.Succcess))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(result));
            }
        }
    }
}
