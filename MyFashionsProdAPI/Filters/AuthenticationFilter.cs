﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;

namespace MyFashionsProdAPI.Filters
{
    public class AuthenticationFilter : BasicAuthenticationFilter
    {
        public AuthenticationFilter()
        { }

        public AuthenticationFilter(bool active)
            : base(active)
        { }

        protected override bool OnAuthorizeUser(string username, string password, string adminType, HttpActionContext actionContext)
        {
            object tempObj = null;
            LoginInfoCache loginCache = new LoginInfoCache();
            string errorMsg = string.Empty;
            string result = string.Empty;
            switch (adminType)
            {
                case "A"://A - Super Admin Login
                    long superAdminID = 0;
                    SuperAdminLogic superAdmin = new SuperAdminLogic();
                    result = superAdmin.ValidateSuperAdmin(username, password, out superAdminID);
                    if (result.Equals(Messages.Succcess))
                    {
                        loginCache.Login = LoginType.SuperAdmin;

                        if (actionContext.Request.Properties.TryGetValue("HeaderInfo", out tempObj))
                        {
                            actionContext.Request.Properties.Remove("HeaderInfo");
                        }
                        actionContext.Request.Properties.Add(new KeyValuePair<string, object>("HeaderInfo", loginCache));
                    }
                    break;
                case "C"://C - Company Login
                    long companyID = 0;
                    CompanyOperationsLogic companyOper = new CompanyOperationsLogic();
                    result = companyOper.ValidateCompanyLogin(username, password, out companyID);
                    if (result.Equals(Messages.Succcess))
                    {
                        Company companyInfo = companyOper.GetCompanyInfo(companyID, out errorMsg);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            loginCache.CompanyID = companyInfo.ID;
                            loginCache.CompanyName = companyInfo.NAME;
                            List<Stores> lstStores = (new StoreOperationsLogic(loginCache).GetAllStores(companyInfo.ID));
                            loginCache.LstStoreIds = (lstStores != null && lstStores.Count > 0) ? lstStores.Select(c => c.ID).ToList() : new List<long>();
                            loginCache.Login = LoginType.Company;

                            if (actionContext.Request.Properties.TryGetValue("HeaderInfo", out tempObj))
                            {
                                actionContext.Request.Properties.Remove("HeaderInfo");
                            }
                            actionContext.Request.Properties.Add(new KeyValuePair<string, object>
                                ("HeaderInfo", loginCache));
                            return true;
                        }
                    }
                    break;
                case "S"://S - Store Login
                    long storeID = 0;
                    StoreOperationsLogic storeOper = new StoreOperationsLogic(loginCache);
                result = storeOper.ValidateStoreLogin(username, password, out storeID);
                if (result.Equals(Messages.Succcess))
                {
                    Stores storeInfo = storeOper.GetStoreInfo(storeID, out errorMsg);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        loginCache.CompanyID = storeInfo.REGISTEREDCOMPANY.ID;
                        loginCache.CompanyName = storeInfo.REGISTEREDCOMPANY.NAME;
                        loginCache.Login = LoginType.Store;
                        //LoginInfoCache.StoreID = storeInfo.ID;
                        loginCache.LstStoreIds.Add(storeInfo.ID);
                        loginCache.StoreName = storeInfo.NAME;

                        if (actionContext.Request.Properties.TryGetValue("HeaderInfo", out tempObj))
                        {
                            actionContext.Request.Properties.Remove("HeaderInfo");
                        }
                        actionContext.Request.Properties.Add(new KeyValuePair<string, object>
                            ("HeaderInfo", loginCache));
                        return true;
                    }
                }
                break;
                default:
                    return false;
            }
            return false;
        }
    }
}