﻿using MyfashionsDB.Models;
using MyFashionsProd.API.BusinessLayer;
using MyFashionsProd.API.BusinessLayer.AssignValues;
using MyFashionsProd.API.BusinessLayer.Categories;
using MyFashionsProd.API.BusinessLayer.Filters;
using MyFashionsProd.API.BusinessLayer.Offers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APITestApplication
{
    public partial class Form1 : Form
    {
        string username = "admin";
        string password = "myfashions";
        string frameURI = "http://localhost:52895/";
        HttpClient client = new HttpClient();

        public Form1()
        {
            InitializeComponent();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string authentication = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes("ADMIN1:123456"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authentication);
            client.DefaultRequestHeaders.Add("AdminType", "S");
            Uri baseAddress = new Uri(frameURI);
            client.BaseAddress = baseAddress;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string uri = "api/SuperAdmin/ValidateSuperAdmin?username=" + username + "&password=" + password;
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {

            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string uri = "api/SuperAdmin/InitiateSeed?initiateseed=true";
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {

            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=1";
                
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MainEnum main = response.Content.ReadAsAsync<MainEnum>().Result;
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Company comp = new Company();
            comp.ID = 1;
            comp.USERNAME = "test username";
            comp.PASSWORD = "test password";
            comp.VERSION = 1;
            comp.LASTUPDATEDTIME = DateTime.Now;
            comp.REGISTEREDSUPERADMIN = new SuperAdmin() { ID = 1 };

            //string uri = "api/CompanyLogin/CreateCompany?dummyparam=true";
            string uri = "api/CompanyLogin/CompanyCRUDOperation?parameter=C";
            HttpResponseMessage response = client.PostAsJsonAsync<Company>(uri, comp).Result;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Stores storeInfo = new Stores();
            storeInfo.ID = 1;
            storeInfo.USERNAME = "test username";
            storeInfo.PASSWORD = "test password";
            storeInfo.VERSION = 1;
            storeInfo.LASTUPDATEDTIME = DateTime.Now;
            storeInfo.ISACTIVE = true;
            storeInfo.NAME = "asfd";
            storeInfo.NUMEMPLOYEES = 25;
            storeInfo.STOREID = "A123";
            storeInfo.IMAGELOCATION = @"C:\asdf.png";
            storeInfo.REGISTEREDCOMPANY = new Company() { ID = 1, USERNAME = "SRINIVAS" };

            //string uri = "api/CompanyLogin/CreateCompany?dummyparam=true";
            string uri = "api/StoreLogin/StoreCRUDOperation?parameter=c";
            HttpResponseMessage response = client.PostAsJsonAsync<Stores>(uri, storeInfo).Result;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MasterOffers masterOffer = Apps.dbContext.MasterOffers.FirstOrDefault();

            MasterOffers mainMasterOffer = new MasterOffers();
            mainMasterOffer = PopulateOfferFields.AssignFieldsForCreateOffer(mainMasterOffer, masterOffer);
            mainMasterOffer.REGCOMPANY = masterOffer.REGCOMPANY;
            List<Stores> lstStores = Apps.dbContext.Stores.OrderBy(c=>c.ID).Skip(1).Take(2).ToList();
            mainMasterOffer.LSTREGSTORE.AddRange(lstStores);
            mainMasterOffer.LASTUPDATEDTIME = DateTime.Now;

            MasterOffersOperationsLogic masterOfferLogic = new MasterOffersOperationsLogic();
            string result = masterOfferLogic.UpdateOffer(mainMasterOffer);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            MasterCategories masterCateg = Apps.dbContext.MasterCategories.FirstOrDefault(c => c.ID.Equals(8));
            masterCateg.NAME = "MEN SUITS";
            MasterCategoriesOperationsLogic masterCategOperations = new MasterCategoriesOperationsLogic();
            masterCategOperations.UpdateMasterCategory(masterCateg);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            MasterFilters masterFilter = Apps.dbContext.MasterFilters.FirstOrDefault(c => c.ID.Equals(9));
            masterFilter.NAME = "22 CTS";
            MasterFiltersOperationsLogic masterFilterOperations = new MasterFiltersOperationsLogic();
            masterFilterOperations.UpdateFilter(masterFilter);
        }
    }
}
