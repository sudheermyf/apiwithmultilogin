﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.Models.Enums
{
    public enum ExecutedFrom
    {
        Category,
        Offers,
        NewArrivals
    }
}
