﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.Models.PriceRange
{
    public class PriceSlider
    {
        public decimal StartPrice { get; set; }
        public decimal EndPrice { get; set; }
    }
}
