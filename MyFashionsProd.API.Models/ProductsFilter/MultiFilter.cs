﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyFashionsProd.API.Models.ProductsFilter
{
    public class MultiFilter
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public MultiFilter(long id, string name)
        {
            // TODO: Complete member initialization
            Id = id;
            Name = name;
        }
    }
}
