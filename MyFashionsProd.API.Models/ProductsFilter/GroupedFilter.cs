﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.Models.ProductsFilter
{
    public class GroupedFilter
    {
        public string CategoryName { get; set; }
        public List<MultiFilter> LstMultiFilter { get; set; }
    }
}
