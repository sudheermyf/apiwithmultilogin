﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFashionsProd.API.Models.ProductsFilter
{
    public class IntegratedFilter
    {
        public long Id { get; set; }
        public decimal StartPrice { get; set; }
        public decimal EndPrice { get; set; }
        public long DefaultStoreId { get; set; }
        //This will use when execute from New Arrivals
        public long CategoryID { get; set; }
        public Enums.ExecutedFrom ExecutionFrom { get; set; }

        public List<GroupedFilter> LstGroupedFilters { get; set; }
    }
}
